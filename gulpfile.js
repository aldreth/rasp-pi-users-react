require('babel/register');

var dotenv =  require('dotenv');
dotenv.config({silent: true});

require('./get-config')();

var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var devWebpackConfig = require('./webpack.config');
var prodWebpackConfig = require('./webpack.production.config');
var requireDir = require('require-dir');
var shell = require('gulp-shell');
var eslint = require('gulp-eslint');
var cached = require('gulp-cached');
var remember = require('gulp-remember');
var spawn = require('child_process').spawn;

// Fetch all the gulp tasks split in to separate modules
requireDir('./tasks');

var server;

// `gulp` - Provides a live reloading development enviornment
gulp.task('default', ['serve:devserver', 'watch:build:devClient']);

// part of the default task, provides a watch over the server side code, on a change it will run the `server` task
gulp.task('serve:devserver', ['server'], function() {
  gulp.watch(['./src/**/*.js', './src/**/*.jsx'], ['server']);
});

// Provides a reloading node server for the development environment
gulp.task('server', ['mb:create-imposters', 'transpile'], function() {
  if (server) {
    server.kill();
  }
  server = spawn('node', ['./build/server/index.js'], {stdio: 'inherit'});
  server.on('close', function close(code) {
    if (code === 8) {
      gutil.log('Error detected, waiting for changes...');
    }
  });
});

// Linting for all the javascript in the application
gulp.task('lint', function() {
  return gulp
    .src(['./src/**/*'])
    .pipe(cached('lint'))
    .pipe(eslint({
      useEslintrc: true,
    }))
    .pipe(remember('lint'))
    .pipe(eslint.format());
});

// `gulp build:prod` - used in the production environment to build the assets for the production env
gulp.task('build:prod', ['build:prodClient', 'transpile']);

// Serves a test version of the application after building the *productionised* client and server stack
gulp.task('serve:test', ['mb:start', 'build:prodClient', 'transpile'], function() {
  process.env.NODE_ENV = 'devtest';
  global.testServer = require('./build/server/index.js');
});

// Deletes the build folder to give us a clean directory to transpile to
gulp.task('reset:build', shell.task([
  'rm -rf ./build/',
]));

// Transpiles all of the code using babel so we can use ES6 everywhere
gulp.task('transpile', ['index:instructions', 'reset:build', 'lint'], function() {
  return gulp
    .src([
      'src/**/*',
    ], { base: 'src' })
    .pipe(sourcemaps.init())
    .pipe(cached('babel'))
    .pipe(babel())
    .pipe(remember('babel'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build'))
  ;
});

// The webpack task to build the development client, this includes source maps and is unminified
gulp.task('watch:build:devClient', ['index:instructions', 'lint'], function() {
  // run webpack with development config
  webpack(devWebpackConfig, function(err, stats) {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString('minimal'));
  });
});

// The webpack task to build the *PRODUCTION* client, source is minified and includes no source maps
gulp.task('build:prodClient', ['transpile'], function(done) {
  // run webpack with prodution config
  webpack(prodWebpackConfig, function(err, stats) {
    if (err) throw new gutil.PluginError('webpack', err);
    gutil.log('[webpack]', stats.toString('minimal'));
    done();
  });
});

// `gulp watch:test` a watch task to run the jest tests anytime it detects a change to the javascript source
gulp.task('watch:test', function() {
  gulp.watch(['src/**', 'tests/**'], ['jest']);
});

// On exit just make sure we kill the dev server with prejudice
process.on('exit', function() {
  if (server) server.kill();
});
