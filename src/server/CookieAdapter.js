export default class CookieAdapter {

  constructor(context) {
    this.context = context;
  }

  all() {
    var cookies = [];
    if (this.context.headers.cookie) {
      cookies = this.context.headers.cookie.split(';');
    }

    var parsedCookies = {};

    cookies.forEach((cookie) => {
      var cookieArray = cookie.split('=');

      var key = cookieArray[0].trim();

      parsedCookies[key] = this.context.cookies.get(key);
    });

    return parsedCookies;
  }

  /**
   * @param {String} name
   * @param {String} value
   * @param {Object} options = {}
   * * `signed` sign the cookie value
   * * `expires` a Date for cookie expiration
   * * `path` cookie path, /' by default
   * * `domain` cookie domain
   * * `secure` secure cookie
   * * `httpOnly` server-accessible cookie, true by default
   */
  set(name, value, options = {}) {
    this.context.cookies.set(name, value, options);
  }

  remove(name) {
    this.context.cookies.set(name, '', new Date('Thu, 01 Jan 1970 00:00:01 GMT;'));
  }

}
