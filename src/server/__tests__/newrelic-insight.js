jest.dontMock('../../../get-config');
jest.dontMock('../newrelic-insight');

const appConfig = require('../../../get-config')();
const fetch = require('isomorphic-fetch');
const NewrelicInsight = require('../newrelic-insight');
const newrelicInsight = new NewrelicInsight({noticeError: () => {}});

const defaultEventType = 'Warning';
const customEventType = 'Custom';
const insightHeader = 'X-Insert-Key';
const testLocation = 'jest test';

describe('Newrelic insight reporting', () => {
  afterEach(() => {
    fetch.mockClear();
  });

  it('processes insight data as expected, populating default eventType and environments if omitted', () => {
    const data = [
      {
        location: testLocation,
      },
    ];
    newrelicInsight.notify(data);

    const call = fetch.mock.calls[0];

    const url = call[0];
    expect(url).toEqual(appConfig.insightUrl);

    const headers = call[1].headers;
    expect(headers[insightHeader]).toEqual(appConfig.insightKey);

    const requestData = call[1].body[0];
    expect(requestData.eventType).toEqual(defaultEventType);
    expect(requestData.environment).toEqual(process.env.NODE_ENV);
    expect(requestData.location).toEqual(testLocation);
  });

  it('accetps a custom event type', () => {
    const data = [
      {
        eventType: customEventType,
      },
    ];
    newrelicInsight.notify(data);

    const call = fetch.mock.calls[0];
    const requestData = call[1].body[0];
    expect(requestData.eventType).toEqual(customEventType);
  });

  it('still works when sent a raw object, auto-wrapping it in an array', () => {
    const data = {
      location: testLocation,
    };
    newrelicInsight.notify(data);

    const call = fetch.mock.calls[0];
    const requestData = call[1].body[0];
    expect(requestData.location).toEqual(testLocation);
  });
});
