import fetch from 'isomorphic-fetch';
import newrelic from 'newrelic';
import NewrelicInsight from '../newrelic-insight';
import getConfig from '../../../get-config';
import { status, json } from '../../helpers/fetch';

const appConfig = getConfig();
const newrelicInsight = new NewrelicInsight(newrelic);

/**
 * Proxy for trial checks
 */
function* widebandApi() {
  const mcs = appConfig.mcsEndpoint;
  const tkn = '?consumer=' + encodeURIComponent(appConfig.mcsConsumerKey);
  const url = mcs + this.params.sso + tkn;

  this.body = yield fetch(url)
    .then(status)
    .then(json)
    .then(mcsData => mcsData.isWidebandEnabled)
    .catch(err => {
      // if something went wrong
      const location = 'MCS Trial Check';
      const message = err.message || 'Unknown Error';

      if (err.response && err.response.status === 401) {
        const data = {
          location: location + ', catch 401',
          message,
        };
        newrelicInsight.notify(data);
        console.log(location + ' Warning: ' + data.message);

        throw new Error('Unauthorized');
      } else {
        const newErr = new Error('Unauthorized');
        newrelic.noticeError(newErr, {location: location + ', catch'});
        console.log(location + ' Error: ' + message);

        throw newErr;
      }
      // What to do exactly in case of MCS Service Unauthorised / Error
      this.status = 401;

      return 'Unauthorized';
    });
}

export default widebandApi;
