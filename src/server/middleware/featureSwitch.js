import CookieJar from '../../helpers/CookieJar';
import ServerCookieAdapter from '../CookieAdapter';
import FeatureSwitch from '../../helpers/FeatureSwitch';
import getConfig from '../../../get-config';

var appConfig = getConfig();

export default function* featureSwitch(next) {
  var cookieJar = new CookieJar(new ServerCookieAdapter(this));

  this.state.cookieJar = cookieJar;
  this.state.features = new FeatureSwitch(appConfig.features, cookieJar);

  yield next;
}
