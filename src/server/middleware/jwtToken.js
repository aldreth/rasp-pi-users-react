import getConfig from '../../../get-config';
var appConfig = getConfig();

import jwt from 'koa-jwt';
import newrelic from 'newrelic';
import NewrelicInsight from '../newrelic-insight';
var newrelicInsight = new NewrelicInsight(newrelic);

const jwtSecret = appConfig.jwtSecret;


// Set JWT token
function* setJwtToken(next) {
  if (this.url.match(/^(?:\/?|\/home\/?)$/)) {
    var claims = {access: 'allowed'};
    var token = jwt.sign(claims, jwtSecret, {expiresInMinutes: 60});
    this.state.cookieJar.set('jwt-token', token, {httpOnly: false});
  }
  yield next;
}

// If JWT token is invalid, 401 Unauthorized error is raised
// and app is redirected to home
function* handleJwtTokenError(next) {
  try {
    yield next;
  } catch (err) {
    var location = 'handleJwtTokenError';
    var message = err.message || 'Unknown Error';

    if (err.status === 401) {
      var data = {
        location: location + ', catch',
        message: message,
      };
      newrelicInsight.notify(data);
      console.log(location + ' Warning : ' + message);
      this.status = 307;
      this.redirect('/');
    } else {
      newrelic.noticeError(err, {location: 'handleJwtTokenError'});
      console.log(location + ' Error : ' + message);
      throw err;
    }
  }
}

export {
  setJwtToken,
  handleJwtTokenError,
};
