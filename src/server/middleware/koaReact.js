import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { match, RoutingContext } from 'react-router';
import newrelic from 'newrelic';

import AppContext from '../../shared/components/AppContext';
import routes from '../../shared/routes';
import NotFoundException from '../../shared/routing/NotFoundException';

export default function* koaReact(next) {
  const myUrl = this.url;
  const notFoundUrl = 'http://www.sky.com/skycom/404';

  try {
    this.state.content = yield new Promise((resolve) => {
      match({ routes, location: myUrl }, (err, redirectLocation, renderProps) => {
        if (err) {
          throw err;
        } else if (redirectLocation) {
          this.status = 307;
          this.redirect(redirectLocation.pathname + redirectLocation.search);
        } else if (renderProps) {
          var content = ReactDOMServer.renderToString(
            <AppContext
              cookieJar={this.state.cookieJar}
              store={this.state.store}
            >
            <RoutingContext {...renderProps} />
            </AppContext>
          );
          resolve(content);
        } else {
          throw new NotFoundException('No route found');
        }
      });
    });
  } catch (err) {
    console.log('SERVER ERROR: react-router: ' + this.request.url + ' ' + err.stack);
    newrelic.noticeError(err, {location: 'KoaReact catch'});

    if (err instanceof NotFoundException) {
      this.status = 307;
      this.redirect(notFoundUrl);
    }
  }

  yield next;
}
