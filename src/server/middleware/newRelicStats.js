import newrelic from 'newrelic';

function* newRelicStats(next) {
  const url = this.url.substr(0, this.url.indexOf('?') !== -1 ? this.url.indexOf('?') : this.url.length);
  newrelic.setControllerName(url);
  yield * next;
}

export default newRelicStats;
