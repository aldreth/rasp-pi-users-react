import getConfig from '../../../get-config';
var appConfig = getConfig();

import DocumentTitle from 'react-document-title';
import newrelic from 'newrelic';

const analyticsAcct = appConfig.analyticsAcct;
const googleAnalyticsId = appConfig.googleAnalyticsId;

function* renderTemplate(next) {
  // https://www.npmjs.com/package/react-document-title#server-usage
  var title = DocumentTitle.rewind();

  yield this.render('index', {
    content: this.state.content,
    store: this.state.store.getState(),
    title: title,
    newRelicBrowserTracking: newrelic.getBrowserTimingHeader(),
    analyticsAcct: analyticsAcct,
    googleAnalyticsId: googleAnalyticsId,
  });

  yield next;
}

export default renderTemplate;
