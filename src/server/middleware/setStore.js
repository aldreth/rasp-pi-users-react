import getStore from '../../shared/getStore';
import getConfig from '../../../get-config';

var appConfig = getConfig();

function* setStore(next) {
  let cookieState;
  try {
    cookieState = this.state.cookieJar.get(appConfig.stateCookie);
    cookieState = JSON.parse(new Buffer(cookieState, 'base64').toString('ascii'));
  } catch (err) {
    cookieState = false;
  }

  const initialState = cookieState || {
    features: this.state.features.all(),
  };

  this.state.store = getStore(initialState);

  yield next;
}

export default setStore;
