import getConfig from '../../../get-config';
var appConfig = getConfig();

import instructions from '../../shared/components/instructions';

function* redirectTo404(next) {
  const isInstruction = this.request.url.substr(0, 1) === '/';
  const urlInstruction = this.request.url.substr(1);

  if (isInstruction && urlInstruction && !instructions[urlInstruction]) {
    this.status = 307;
    this.redirect('http://www.sky.com/skycom/404');
  } else {
    yield next;
  }
}

function* redirectLegacyDeepLinks(next) {
  if (appConfig.legacyDeepLinks.indexOf(this.request.url) > -1) {
    this.status = 307;
    this.redirect('/');
  } else {
    yield next;
  }
}

export function* redirectTrialSteps(next) {
  const { features, user } = this.state.store.getState();

  if (
    !(appConfig.trialSteps.indexOf(this.request.url) > -1) ||
    (features.isTrialEnabled && user && user.isTrial)
  ) {
    yield next;
  } else {
    this.redirect('/');
  }
}

export {
  redirectTo404,
  redirectLegacyDeepLinks,
  redirectTrialSteps,
};
