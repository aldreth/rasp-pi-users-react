/**
 * babel re-orders imports to be at the top, so any globals / polyfills defined in index.js
 * wouldn't be defined until after all dependencies have been resolved
 */
global.IS_CLIENT = false;
