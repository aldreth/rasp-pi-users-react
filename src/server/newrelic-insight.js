import fetch from 'isomorphic-fetch';
import _ from 'lodash';
import { status } from '../helpers/fetch';
import getConfig from '../../get-config';
var appConfig = getConfig();

class NewrelicInsight {

  constructor(newrelic) {
    this.newrelic = newrelic;
    this.defaultType = 'Warning';
  }

  // Can only send an array of shallow key:value pair objects
  // Multiple events can be sent to insight in one call
  // Each event in the array must have an 'eventType' under
  // which NewRelic reports
  notify(data) {
    const defaultType = this.defaultType;
    const newrelic = this.newrelic;

    let jsonData = data;

    if ( !_.isArray(data) ) {
      jsonData = [data];
    }

    jsonData.map(function prefix(event) {
      event.eventType = event.eventType || defaultType;
      event.environment = event.environment || process.env.NODE_ENV;
    });

    fetch(appConfig.insightUrl, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        'X-Insert-Key': appConfig.insightKey,
      },
      body: jsonData,
    })
      .then(status)
      .catch(err => newrelic.noticeError(err, {location: 'Insight reporting'}));
  }
}

export default NewrelicInsight;
