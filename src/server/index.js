import getConfig from '../../get-config';
import './globals';
import 'babel/polyfill';

import koa from 'koa';
import http from 'http';
import hbs from 'koa-hbs';
import serve from 'koa-static';
import path from 'path';
import koaRouter from 'koa-router';
import compress from 'koa-compress';
import Promise from 'bluebird';

// MIDDLEWARE
import koaReact from './middleware/koaReact';
import widebandApi from './middleware/widebandApi';
import setStore from './middleware/setStore';
import featureSwitch from './middleware/featureSwitch';
import renderTemplate from './middleware/renderTemplate';
import newRelicStats from './middleware/newRelicStats';

global.Promise = Promise;

const appConfig = getConfig();
const app = koa();
const api = koaRouter();

app.use(newRelicStats);

app.use(compress());

// ... set base document directory, and ...
app.use(serve(path.resolve(__dirname, '../../public')));

app.use(featureSwitch);

// populate the redux store
app.use(setStore);

api.get('/ping', function* pingResponse() {
  this.body = 'pong';
});

// Let Koa Router handle ajax / api calls
api.get('/wideband/:sso', widebandApi);

app.use(api.routes());

// For all other routes use handlebars templates middleware, and ...
hbs.registerHelper('json', obj => JSON.stringify(obj));
app.use(hbs.middleware({
  viewPath: appConfig.viewsPath,
  partialsPath: appConfig.partialsPath,
}));

// ... let React-Router handle all page serving routes
app.use(koaReact);

// Render the template
app.use(renderTemplate);

// Listen
var port = appConfig.port;
var server = http.createServer(app.callback()).listen(port);
console.log('Listening on port : ' + (port));

app.shutdown = () => {
  console.log('Gracefully shutting down');
  server.close();
};

export default app;
