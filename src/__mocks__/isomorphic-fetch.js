const fetch = jest.genMockFromModule('isomorphic-fetch');

fetch.mockReturnValue(Promise.resolve({
  status: 200,
  statusText: 'OK',
  json: () => JSON.parse('{}'),
}));

export default fetch;
