import fetch from 'isomorphic-fetch';
import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import update from 'react-addons-update';
import DocumentTitle from 'react-document-title';

import getConfig from '../../../get-config';
import { createUser } from '../../helpers/users';

const appConfig = getConfig();


class NewUser extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
  }

  constructor(...props) {
    super(...props);

    this.state = {
      userInfo: {
        username: '',
        email: '',
        password: '',
      },
    };
  }

  componentDidMount() {
  //   // this.props.dispatch(users());
  }

  getTitle(readableInstruction) {
    return 'Katana' + (readableInstruction ? ' | ' + readableInstruction : '');
  }

  tryToFetch() {
    const url = `http://${appConfig.apiUrl}/users`;
    const user = {
      user: {
        username: 'edasasdfadf444ward',
        email: 'ed3asdf33ward@aldreth.com',
        password: 'password1234',
      },
    };

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    createUser(this.state.userInfo);
  }

  handleInputChange(event, key) {
    const userInfo = update(this.state.userInfo, {
      [key]: {$set: event.target.value},
    });

    this.setState({ userInfo: userInfo });
  }

  render() {
    const pageTitle = this.getTitle('New user');
    return (
        <div className="eight centered columns">
          <DocumentTitle title={pageTitle} />
          <h2>Users</h2>
          <form className="commentForm" onSubmit={(event) => this.handleSubmit(event)}>
            <input
               type="text"
               placeholder="Username"
               value={this.state.userInfo.username}
               onChange={(value) => this.handleInputChange(value, 'username')}
             />
            <input
               type="text"
               placeholder="Email"
               value={this.state.userInfo.email}
               onChange={(value) => this.handleInputChange(value, 'email')}
             />
             <input
               type="text"
               placeholder="Password"
               value={this.state.userInfo.password}
               onChange={(event) => this.handleInputChange(event, 'password')}
             />
             <input type="submit" value="Save" />
           </form>
        </div>
      );
  }
}

export default connect(state => state)(NewUser);
