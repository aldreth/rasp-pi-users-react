import React from 'react';

export default class AppContext extends React.Component {
  static propTypes = {
    children: React.PropTypes.element.isRequired,
    cookieJar: React.PropTypes.object.isRequired,
    store: React.PropTypes.object.isRequired,
  }

  static childContextTypes = {
    cookieJar: React.PropTypes.object,
    store: React.PropTypes.object,
  }

  getChildContext() {
    return {
      cookieJar: this.props.cookieJar,
      store: this.props.store,
    };
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}
