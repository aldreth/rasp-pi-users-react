import {Component} from 'react';

import NotFoundException from '../routing/NotFoundException';

export default class NotFound extends Component {
  constructor(...args) {
    super(...args);

    // trigger redirect
    throw new NotFoundException('Not found');
  }
}
