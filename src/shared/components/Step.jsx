import React from 'react';
import DocumentTitle from 'react-document-title';

import instructions from '../components/instructions';
import Humanise from '../../helpers/Humanise';
import NotFoundException from '../routing/NotFoundException';
import analyticsPage from '../../helpers/analytics-page';


class Step extends React.Component {
  static propTypes = {
    params: React.PropTypes.object.isRequired,
  };

  constructor(props, context) {
    super(props, context);

    return this.checkInstruction(props.params.instruction);
  }

  componentDidMount() {
    this.trackAnalytics();
  }

  componentWillReceiveProps(nextProps) {
    return this.checkInstruction(nextProps.params.instruction);
  }

  componentDidUpdate() {
    this.trackAnalytics();
  }

  getTitle(readableInstruction) {
    return 'Katana' + (readableInstruction ? ' | ' + readableInstruction : '');
  }

  trackAnalytics() {
    const readableInstruction = Humanise.parse(this.props.params.instruction);
    const pageTitle = this.getTitle(readableInstruction);

    analyticsPage(pageTitle, readableInstruction);
  }

  checkInstruction(instruction) {
    if (!instructions[instruction]) {
      throw new NotFoundException('Instruction ' + instruction + ' not found');
    }
  }

  render() {
    const instruction = this.props.params.instruction;
    const InstructionComponent = instructions[instruction];
    const readableInstruction = Humanise.parse(instruction);
    const pageTitle = this.getTitle(readableInstruction);

    return (
      <div id="step-container">
        <DocumentTitle title={pageTitle}>
          <InstructionComponent params={this.props.params} />
        </DocumentTitle>
      </div>
    );
  }
}

export default Step;
