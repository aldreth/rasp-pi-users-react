import React, { PropTypes, Component } from 'react';
import {Link} from 'react-router';

class UserLink extends Component {
  static propTypes = {
    displayUser: PropTypes.object.isRequired,
  }

  render() {
    return (
        <dl>
          <dt>
            <Link to={`/users/${this.props.displayUser.id}`} >
              {this.props.displayUser.username}
            </Link>
            </dt>
          <dd>{this.props.displayUser.email}</dd>
        </dl>
      );
  }
}

export default UserLink;
