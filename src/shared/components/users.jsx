import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import UserLink from './user-link';

import { users} from '../actions/user';

class Users extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
  }

  componentDidMount() {
    this.props.dispatch(users());
  }

  getTitle(readableInstruction) {
    return 'Katana' + (readableInstruction ? ' | ' + readableInstruction : '');
  }

  render() {
    const displayUsers = this.props.user.users.map((displayUser, idx) => {
      return (
        <UserLink displayUser={displayUser} key={idx}/>
      );
    });
    const pageTitle = this.getTitle('All users');

    return (
        <div className="eight centered columns">
          <DocumentTitle title={pageTitle} />
          <h2>Users</h2>
          {displayUsers}
        </div>
      );
  }
}

export default connect(state => state)(Users);
