import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import DocumentTitle from 'react-document-title';

import { user } from '../actions/user';

class User extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
  }

  componentDidMount() {
    this.props.dispatch(user(this.props.params.id));
  }

  getTitle(readableInstruction) {
    return 'Katana' + (readableInstruction ? ' | ' + readableInstruction : '');
  }

  title() {
    return 'user';
  }

  render() {
    const pageTitle = this.getTitle(this.props.user.info.username);

    return (
        <div className="eight centered columns">
          <DocumentTitle title={pageTitle} />
          <h1 className="title">{this.props.user.info.username}</h1>
          <p>{this.props.user.info.email}</p>
          <Link to="/users">All users</Link>
        </div>
      );
  }
}

export default connect(state => state)(User);
