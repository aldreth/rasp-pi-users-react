import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

class Home extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    features: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
  }

  render() {
    return (
      <header className="row header" role="banner">
        <hgroup className="twelve columns">
          <h1 id="logo">
              <img alt="Logo" height="150" src="/assets/logo.svg" />
          </h1>
          <h2 className="eight centered columns"><Link className="users" data-tracking-context="katana" to="/users">See all users</Link></h2>
        </hgroup>
      </header>
    );
  }
}

export default connect(state => state)(Home);
