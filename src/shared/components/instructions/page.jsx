import React from 'react';
import { connect } from 'react-redux';

class Page extends React.Component {
  render() {
    return (
        <div>
          <h1 className="title">Welcome to a page</h1>
        </div>
      );
  }
}

export default connect(state => state)(Page);
