/**
* Expects 3 images to be in the public/assets/img directory, with filenames following this pattern:
* image-phone.jpg, image-tablet.jpg & image-desktop.jpg
* These files can be generated from one original image by using the gulp resize-images task.
* Markup from http://scottjehl.github.io/picturefill/#using-picture
*/


import React from 'react';

const Picture = React.createClass({
  propTypes: {
    imageLocation: React.PropTypes.string.isRequired,
  },

  render: function render() {
    const [img, filetype] = this.props.imageLocation.split('.');

    return (
      <picture>
        <source srcSet={'/assets/img/' + img + '-phone.' + filetype} media="(max-width: 767px)" />
        <source srcSet={'/assets/img/' + img + '-tablet.' + filetype} media="(max-width: 992px) and (min-width: 768px)" />
        <source srcSet={'/assets/img/' + img + '-desktop.' + filetype} media="(min-width: 993px)" />
        <img srcSet={'/assets/img/' + img + '-desktop.' + filetype} />
      </picture>
    );
  },
});

export default Picture;
