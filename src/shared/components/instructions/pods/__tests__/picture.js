jest.dontMock('../picture');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var Picture = require('../picture');

describe('picture component', () => {
  it('renders scrsets & img correctly', () => {
    var testImage = 'image.jpg';

    var pictureComponent = TestUtils.renderIntoDocument(
      <Picture imageLocation={testImage} />
    );

    var picture = TestUtils.findRenderedDOMComponentWithTag(pictureComponent, 'picture');
    var html = ReactDOM.findDOMNode(picture).innerHTML;

    expect(html).toContain('source srcset="/assets/img/image-phone.jpg" media="(max-width: 767px)"');
    expect(html).toContain('source srcset="/assets/img/image-tablet.jpg" media="(max-width: 992px) and (min-width: 768px)"');
    expect(html).toContain('source srcset="/assets/img/image-desktop.jpg" media="(min-width: 993px)"');
    expect(html).toContain('img srcset="/assets/img/image-desktop.jpg"');
  });
});
