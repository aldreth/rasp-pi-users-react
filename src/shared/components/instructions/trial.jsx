import React from 'react';
import { connect } from 'react-redux';

class Page extends React.Component {
  render() {
    return (
        <div>
          <h1 className="step-header">Welcome to a trial page</h1>
        </div>
      );
  }
}

export default connect(state => state)(Page);
