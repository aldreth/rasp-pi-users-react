jest.dontMock('../trial');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var Component = require('../trial');
var reactRouterContext = require('../../../../../tests/support/reactRouterContext');
var Subject = reactRouterContext(Component);
var subject;

describe('Done', () => {
  beforeEach(() => {
    subject = TestUtils.renderIntoDocument(<Subject />);
  });

  it('renders the trial component', () => {
    var Element = TestUtils.findRenderedDOMComponentWithClass(subject, 'step-header');
    expect(ReactDOM.findDOMNode(Element).textContent).toEqual('Welcome to a trial page');
  });
});
