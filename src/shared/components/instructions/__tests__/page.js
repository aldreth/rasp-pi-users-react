jest.dontMock('../page');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var Component = require('../page');
var reactRouterContext = require('../../../../../tests/support/reactRouterContext');
var Subject = reactRouterContext(Component);
var subject;

describe('Done', () => {
  beforeEach(() => {
    subject = TestUtils.renderIntoDocument(<Subject />);
  });

  it('renders the page component', () => {
    var Element = TestUtils.findRenderedDOMComponentWithClass(subject, 'title');
    expect(ReactDOM.findDOMNode(Element).textContent).toEqual('Welcome to a page');
  });
});
