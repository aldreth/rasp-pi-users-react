import React from 'react';
import { connect } from 'react-redux';

class Whitelisted extends React.Component {
  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
  }

  render() {
    return (
      <div>
        <h1 className="step-header">A whitelisted page</h1>
        <p>This page can be linked to directly. It is not necessary to visit the home page to reach this page.</p>
        <p>To test this yourself, clear all your cookies and refresh the page. You will still be able to see this page.</p>
      </div>
    );
  }
}

export default connect(state => state)(Whitelisted);
