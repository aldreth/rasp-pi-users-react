jest.dontMock('../features');

const { SET_FEATURE, SET_FEATURES, setFeature, setFeatures } = require('../features');

describe('Feature Switches action creator', () => {
  it('creates an action to set a single feature switch', () => {
    const expectedAction = {
      type: SET_FEATURE,
      payload: {
        key: 'myFeature',
        value: true,
      },
    };

    expect(setFeature('myFeature', true)).toEqual(expectedAction);
  });

  it('creates an action to set multiple feature switches', () => {
    const expectedAction = {
      type: SET_FEATURES,
      payload: {
        myFeature: true,
        myOtherFeature: false,
      },
    };

    expect(setFeatures({myFeature: true, myOtherFeature: false})).toEqual(expectedAction);
  });
});
