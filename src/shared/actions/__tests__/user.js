jest.dontMock('../user');

const { setUser, setUsers } = require('../../../helpers/users');
const { USER, user, USERS, users } = require('../user');

describe('User action creator', () => {
  it('creates an action to get a single user', () => {
    const mockPromise = new Promise(() => {});
    setUser.mockReturnValue(mockPromise);

    const action = user('id');
    expect(action.type).toEqual(USER);
    expect(action.payload).toEqual(mockPromise);
  });

  it('creates an action to get all users', () => {
    const mockPromise = new Promise(() => {});
    setUsers.mockReturnValue(mockPromise);

    const action = users();
    expect(action.type).toEqual(USERS);
    expect(action.payload).toEqual(mockPromise);
  });
});
