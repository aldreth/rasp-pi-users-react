/**
 * action types
 */

export const SET_FEATURE = 'SET_FEATURE';
export const SET_FEATURES = 'SET_FEATURES';

/**
 * action creators
 */

export function setFeature(feature, value) {
  return {
    type: SET_FEATURE,
    payload: {
      key: feature,
      value,
    },
  };
}

export function setFeatures(features) {
  return {
    type: SET_FEATURES,
    payload: features,
  };
}
