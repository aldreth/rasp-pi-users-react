import { setUser, setUsers } from '../../helpers/users';

/**
 * action types
 */

export const USER = 'USER';
export const USERS = 'USERS';

/**
 * action creators
 */

/**
* @param  {String} id
 * @return {Object}
 */
export function user(id) {
  return {
    type: USER,
    payload: setUser(id),
  };
}

/**
 * @return {Object}
 */
export function users() {
  return {
    type: USERS,
    payload: setUsers(),
  };
}
