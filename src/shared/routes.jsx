import {Route} from 'react-router';
import React from 'react';

import App from './app';
import Step from './components/Step';
import User from './components/user';
import NewUser from './components/new-user';
import Users from './components/users';
import NotFound from './components/NotFound';

// Handle default routing to dynamic url segment
function defaultHandler(instruction) {
  return React.createClass({
    render: () => {
      return <Step params={{instruction}}/>;
    },
  });
}

export default (
  <Route component={App}>
    <Route path="/" component={defaultHandler('home')} />
    <Route path="/users" component={Users} />
    <Route path="/users/new" component={NewUser} />
    <Route path="/users/:id" component={User} />
    <Route path="/:instruction" component={Step} />
    <Route path="/:instruction" component={Step} />
    <Route path="*" component={NotFound} />
  </Route>
);
