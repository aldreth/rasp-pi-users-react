import ExtensibleError from 'es6-error';

export default class NotFoundException extends ExtensibleError {
  constructor(message = 'Not found') {
    super(message);
  }
}
