import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import rootReducer from './reducers';

export default function(initialState) {
  const middleware = [thunk, promise];
  let finalCreateStore = applyMiddleware(...middleware)(createStore);

  if (typeof IS_CLIENT !== 'undefined' && IS_CLIENT && window.devToolsExtension) {
    finalCreateStore = compose(
      applyMiddleware(...middleware),
      window.devToolsExtension()
    )(createStore);
  }

  const store = finalCreateStore(rootReducer, initialState);

  return store;
}
