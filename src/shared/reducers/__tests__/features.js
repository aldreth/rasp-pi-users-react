jest.dontMock('../features');
jest.dontMock('../../actions/features');

const features = require('../features');
const { SET_FEATURE, SET_FEATURES } = require('../../actions/features');

describe('Feature Switches reducer', () => {
  it('returns the initial state', () => {
    const expectedState = {};

    expect(features(undefined, {})).toEqual(expectedState);
  });

  it('handles the SET_FEATURE action', () => {
    const expectedState = {
      myFeature: true,
    };

    expect(features({ myFeature: false }, {
      type: SET_FEATURE,
      payload: {
        key: 'myFeature',
        value: true,
      },
    })).toEqual(expectedState);
  });


  it('handles the SET_FEATURES action', () => {
    const expectedState = {
      initialFeature: false,
      myFeature: true,
      myOtherFeature: false,
    };

    expect(features({ initialFeature: false, myFeature: false }, {
      type: SET_FEATURES,
      payload: {
        myFeature: true,
        myOtherFeature: false,
      },
    })).toEqual(expectedState);
  });
});
