jest.dontMock('../user');
jest.dontMock('../../actions/user');

const user = require('../user');
const { USER, USERS } = require('../../actions/user');

const initialState = {
  isFetched: false,
  users: [],
  info: {},
};

describe('User reducer', () => {
  it('returns the initial state', () => {
    expect(user(undefined, {})).toEqual(initialState);
  });

  it('handles the USER action', () => {
    const userJson = {
      id: 1,
      username: 'test',
      email: 'test@example.com',
    };

    const expectedState = {
      isFetched: true,
      users: [],
      info: userJson,
    };

    expect(user(initialState, {
      type: USER,
      payload: userJson,
    })).toEqual(expectedState);
  });

  it('handles the USERS action', () => {
    const userJson = [
      {
        id: 1,
        username: 'test',
        email: 'test@example.com',
      },
      {
        id: 5,
        username: 'test5',
        email: 'test5@example.com',
      },
    ];

    const expectedState = {
      isFetched: true,
      users: userJson,
      info: {},
    };

    expect(user(initialState, {
      type: USERS,
      payload: userJson,
    })).toEqual(expectedState);
  });
});
