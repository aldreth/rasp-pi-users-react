import { combineReducers } from 'redux';
import features from './features';
import user from './user';

const rootReducer = combineReducers({
  features,
  user,
});

export default rootReducer;
