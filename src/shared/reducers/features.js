import { SET_FEATURE, SET_FEATURES } from '../actions/features';

function features(state = {}, action) {
  switch (action.type) {
  case SET_FEATURE:
    return Object.assign({}, state, {
      [action.payload.key]: action.payload.value,
    });
  case SET_FEATURES:
    return Object.assign({}, state, {
      ...action.payload,
    });
  default:
    return state;
  }
}

export default features;
