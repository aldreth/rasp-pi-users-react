import { USER, USERS } from '../actions/user';

const initialState = {
  isFetched: false,
  users: [],
  info: {},
};

function user(state = initialState, action) {
  switch (action.type) {
  case USER:
    // fall back to default on error
    if (action.error) return Object.assign({}, state, { isFetched: true });

    return Object.assign({}, state, {
      isFetched: true,
      info: action.payload,
    });
  case USERS:
    // fall back to default on error
    if (action.error) return Object.assign({}, state, { isFetched: true });

    return Object.assign({}, state, {
      isFetched: true,
      users: action.payload,
    });
  default:
    return state;
  }
}

export default user;
