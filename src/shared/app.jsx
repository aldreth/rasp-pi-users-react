import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';

class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    dispatch: PropTypes.func.isRequired,
    features: PropTypes.object.isRequired,
  }

  static contextTypes = {
    cookieJar: PropTypes.object.isRequired,
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  componentDidUpdate() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default connect(state => state)(App);
