export default class CookieJar {

  _cookies = {};

  /**
   * @param  {DataBag} adapter
   * @return {Cookie}
   */
  constructor(adapter) {
    this.adapter  = adapter;
    this.refresh();
  }

  removeSSO() {
    this.remove('skySSO');
    this.remove('skySSOLast');
  }

  get(name) {
    this.refresh();

    return this._cookies[name];
  }

  set(name, value, options) {
    this.adapter.set(name, value, options);
    this.refresh();
  }

  all() {
    return this._cookies;
  }

  remove(name) {
    this.adapter.remove(name);
    this.refresh();
  }

  refresh() {
    this._cookies = this.adapter.all();
  }

}
