import _ from 'lodash';

class ToSentence {
  // Joins an array of words joined appropriately for a list
  parse(words) {
    return words.length === 1 ? words[0] : `${ _.initial(words).join(', ') } and ${ _.last(words) }`;
  }
}

export default new ToSentence();
