jest.dontMock('../analytics-page');
var analyticsPage = require('../analytics-page');

describe('Analytics Page Population', () => {
  it('pushes objects on to the analytics data layer correctly', () => {
    window.dataLayer = [];
    var pageTitle = 'Katana Test';
    var crumb = 'crumb';

    analyticsPage(pageTitle, crumb);

    expect(window.dataLayer.length).toEqual(4);
    expect(window.dataLayer[1].breadcrumb).toEqual('Katana:' + crumb);
    expect(window.dataLayer[3].digitalData.page.pageInfo.pageName).toEqual(pageTitle);
  });
});
