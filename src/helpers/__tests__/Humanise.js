jest.dontMock('../Humanise');
var Humanise = require('../Humanise');

describe('Humanise', () => {
  it('capitalises the first charater', () => {
    var robot = 'this';
    expect(Humanise.parse(robot)).toEqual('This');
  });

  it('transforms a "-"" into a " "', () => {
    var robot = 'this-is-a-string';
    expect(Humanise.parse(robot)).toEqual('This is a string');
  });

  it('transforms a "_"" into a " "', () => {
    var robot = 'this_is_a_string';
    expect(Humanise.parse(robot)).toEqual('This is a string');
  });

  it('handles an ampersand', () => {
    var robot = 'this-is-a-string-&-another';
    expect(Humanise.parse(robot)).toEqual('This is a string & another');
  });

  it('handles a mixture of spaces, hyphens, and underscores', () => {
    var robot = 'this is_a-string-&-another';
    expect(Humanise.parse(robot)).toEqual('This is a string & another');
  });
});
