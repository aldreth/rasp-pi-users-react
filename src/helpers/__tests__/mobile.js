jest.dontMock('../mobile');

var mobile = require('../mobile');

describe('Mobile', () => {
  it('returns false for desktop devices', () => {
    navigator.__defineGetter__('userAgent', () => {
      return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0'; // customized user agent
    });

    var isDesktop = mobile();
    expect(isDesktop).toEqual(false);
  });

  it('returns true for mobile devices', () => {
    navigator.__defineGetter__('userAgent', () => {
      return 'iPhone/6.0 (Macintosh; iOS 2;)'; // customized user agent
    });

    var isMobile = mobile();
    expect(isMobile).toEqual(true);
  });
});
