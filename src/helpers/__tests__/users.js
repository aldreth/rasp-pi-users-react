// currently the only way to work around promises failing with jest/babel
require('../../../tests/support/promise');

jest.dontMock('../users');
jest.dontMock('../../../get-config');

const Promise = require('bluebird');
const fetch = require('isomorphic-fetch');
const usersApi = require('../users');

function mockResponse(response, fail = false) {
  if (fail) {
    fetch.mockReturnValue(Promise.reject('Request Fail'));
  } else {
    fetch.mockReturnValue(Promise.resolve({
      status: 200,
      statusText: 'OK',
      json: () => response,
    }));
  }
}

describe('usersApi', () => {
  describe('setUser', () => {
    it('returns a single user', (done) => {
      mockResponse({
        id: 1,
        username: 'test',
        email: 'test@example.com',
      });

      return usersApi.setUser(1)
        .then(user => {
          expect(user.id).toEqual(1);
          expect(user.username).toEqual('test');
          expect(user.email).toEqual('test@example.com');
          done();
        });
    });

    it('handles errors', (done) => {
      mockResponse(null, true);

      return usersApi.setUser(1)
        .then(() => {
          expect(false).toEqual(true);
        })
        .catch(err => {
          expect(err).toEqual('Request Fail');
          done();
        });
    });
  });

  describe('setUsers', () => {
    it('returns a list of users', (done) => {
      mockResponse([
        {
          id: 1,
          username: 'test',
          email: 'test@example.com',
        },
        {
          id: 5,
          username: 'test5',
          email: 'test5@example.com',
        },
      ]);

      return usersApi.setUsers()
      .then(users => {
        expect(users.length).toEqual(2);
        expect(users[0].id).toEqual(1);
        expect(users[0].username).toEqual('test');
        expect(users[0].email).toEqual('test@example.com');
        expect(users[1].id).toEqual(5);
        expect(users[1].username).toEqual('test5');
        expect(users[1].email).toEqual('test5@example.com');
        done();
      });
    });

    it('handles errors', (done) => {
      mockResponse(null, true);

      return usersApi.setUsers()
        .then(() => {
          expect(false).toEqual(true);
        })
        .catch(err => {
          expect(err).toEqual('Request Fail');
          done();
        });
    });
  });
});
