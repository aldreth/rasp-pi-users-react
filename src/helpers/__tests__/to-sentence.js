jest.dontMock('../to-sentence');
jest.dontMock('lodash');

var ToSentence = require('../to-sentence');

describe('to-sentence', () => {
  it('returns one word correctly', () => {
    var words = ['one'];
    expect(ToSentence.parse(words)).toEqual('one');
  });

  it('joins two words correctly', () => {
    var words = ['one', 'two'];
    expect(ToSentence.parse(words)).toEqual('one and two');
  });

  it('joins three words correctly', () => {
    var words = ['one', 'two', 'three'];
    expect(ToSentence.parse(words)).toEqual('one, two and three');
  });

  it('joins seven words correctly', () => {
    var words = ['one', 'two', 'three', 'four', 'five', 'six', 'seven'];
    expect(ToSentence.parse(words)).toEqual('one, two, three, four, five, six and seven');
  });
});
