jest.dontMock('../FeatureSwitch');

const FeatureSwitch = require('../FeatureSwitch');
const CookieJar = require('../CookieJar');

describe('FeatureSwitch', () => {
  const cookieJar = new CookieJar();
  cookieJar.all.mockReturnValue({
    'FEATURE_SomeOtherFeature': 'false',
    'notAFeature': 'false',
  });

  const featureSwitch = new FeatureSwitch({
    SomeFeature: true,
  }, cookieJar);

  it('returns all features', () => {
    expect(featureSwitch.all()).toEqual({
      SomeFeature: true,
      SomeOtherFeature: false,
    });
  });

  it('returns a feature', () => {
    expect(featureSwitch.get('SomeOtherFeature')).toEqual(false);
    expect(featureSwitch.get('NotAFeature')).toEqual(undefined);
  });


  it('sets a feature', () => {
    featureSwitch.set('AnotherFeature', true);
    featureSwitch.set({
      YetAnotherFeature: false,
      OneMore: true,
    });

    expect(featureSwitch.all()).toEqual({
      SomeFeature: true,
      SomeOtherFeature: false,
      AnotherFeature: true,
      YetAnotherFeature: false,
      OneMore: true,
    });
  });
});
