jest.dontMock('../fetch');
const fetch = require('../fetch');

describe('Fetch response middleware', () => {
  describe('#status', () => {
    it('passes the response through on a 200-response', () => {
      const response = {
        status: 200,
        statusText: 'OK',
      };

      expect(fetch.status(response)).toEqual(response);
    });

    it('throws an error on a 404-response', () => {
      const response = {
        status: 404,
        statusText: 'Not Found',
      };

      expect(() => fetch.status(response)).toThrowError('Not Found');
    });
  });

  describe('#json', () => {
    it('passes the response\'s .json() result back', () => {
      const json = { 'a': 1 };
      const response = {
        status: 200,
        statusText: 'OK',
        json: () => json,
      };

      expect(fetch.json(response)).toEqual(json);
    });
  });
});
