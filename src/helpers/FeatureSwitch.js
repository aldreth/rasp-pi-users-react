import {chain, extend, mapValues, forEach} from 'lodash';

export default class FeatureSwitch {
  features = {};

  /**
   * @param  {Object} features
   * @param  {CookieJar} cookieJar
   */
  constructor(features, cookieJar) {
    // build switches from config
    const cookieRegex = /^FEATURE_(.+)$/;
    const filteredCookies = chain(cookieJar.all())
      .pick((value, key) => cookieRegex.test(key))
      .mapKeys((value, key) => key.match(cookieRegex)[1])
      .value();
    const extendFeatures = extend({}, features, filteredCookies);

    this.features = mapValues(extendFeatures, JSON.parse);
  }

  /**
   * Get all features
   * @return {Object} A hash of all the features
   */
  all() {
    // extend features by overrides
    return this.features;
  }

  /**
   * Get a feature
   * @param  {String} feature
   * @return {Boolean}
   */
  get(feature) {
    return this.features[feature];
  }

  /**
   * Set a feature
   * @param {String|Object} feature The feature name (can be a hash)
   * @param {Boolean}       value
   */
  set(feature, value) {
    if (typeof feature === 'object') {
      forEach(feature, (val, key) => {
        this.features[key] = val;
      });
    } else {
      this.features[feature] = value;
    }
  }
}
