import fetch from 'isomorphic-fetch';
import { status, json } from './fetch';

/**
* @param  {String} ssoToken
* @return {Promise} Resolves to a Boolean
*/
function trialCheck(ssoToken) {
  if (!ssoToken) {
    // User not logged in
    return Promise.resolve(false);
  }
  const url = '/wideband/' + ssoToken;

  return fetch(url)
    .then(status)
    .then(json)
    .then(body => body === true);
}

export default trialCheck;
