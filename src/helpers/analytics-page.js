/* global ga */
function analyticsPage(pageTitle, readableInstruction) {
  var digitalData = {
    page: {
      pageInfo: {
        pageName: pageTitle,
        destinationURL: window.location.href,
        referringURL: document.referrer,
      },
      category: {
        primaryCategory: 'sky',
        subcategory1: 'help',
        subcategory2: 'katana subcategory2',
      },
    },
    event: [],
  };

  var breadcrumb = 'Katana:' + readableInstruction;
  window.dataLayer.push(function addQueryBreadcrumb() {
    var crumb = this.get('breadcrumb');

    if ( crumb !== undefined ) {
      breadcrumb = crumb + ':' + readableInstruction;
    }
  });

  window.dataLayer.push({ 'breadcrumb': breadcrumb });
  window.dataLayer.push({ 'contentType': 'katana-content-type' });
  window.dataLayer.push({ digitalData: digitalData });

  if (typeof ga !== 'undefined') {
    ga('send', {
      hitType: 'pageview',
      page: window.location.pathname,
      title: pageTitle,
    });
  }
}

module.exports = analyticsPage;
