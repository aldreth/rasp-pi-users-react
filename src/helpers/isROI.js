/**
 * @param  {CookieJar} cookieJar
 * @return {Boolean}
 */
function isROI(cookieJar) {
  return cookieJar.get('geo') === '1%7CIE';
}

export default isROI;
