export const status = jest.genMockFunction();
status.mockImplementation(res => res);

export const json = jest.genMockFunction();
json.mockImplementation(res => res.json());
