function addTrailingSlashes(urlArray) {
  var urlArrayWithSlashes = urlArray.map(function addSlash(link) {
    return link + '/';
  });
  return urlArray.concat(urlArrayWithSlashes);
}

module.exports = addTrailingSlashes;
