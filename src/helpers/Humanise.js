class Humanise {
  // Returns a string that represents the humanised version of the string passed
  parse(value) {
    return value
      .trim()
      .split(/[\s+-]/)
      .map(mappedValue => {
        return mappedValue
          .replace(/_/g, ' ')
          .replace(/\s+/, ' ')
          .trim();
      })
      .join(' ')
      .toLowerCase()
      .replace(/^./, char => char.toUpperCase());
  }
}

export default new Humanise();
