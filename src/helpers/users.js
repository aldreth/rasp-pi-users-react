import fetch from 'isomorphic-fetch';
import { status, json } from './fetch';

import getConfig from '../../get-config';
const appConfig = getConfig();

const createUser = (userInfo) => {
  const url = `http://${appConfig.apiUrl}/users`;
  const user = {
    user: userInfo,
  };

  return fetch(
    url, {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        'Content-Type': 'application/json',
      },
    }
  )
  .then(status);
};

/**
* @return {Promise} Resolves to an object
*/
const setUser = (id) => {
  const url = `http://${appConfig.apiUrl}/users/${id}`;

  return fetch(url)
    // .then((response) => {
    //   console.log("Success!", response);
    // })
    .then(status)
    .then(json);
};

/**
* @return {Promise} Resolves to an object
*/
const setUsers = () => {
  const url = `http://${appConfig.apiUrl}/users`;

  return fetch(url)
    .then(status)
    .then(json);
};

export {
  createUser,
  setUser,
  setUsers,
};
