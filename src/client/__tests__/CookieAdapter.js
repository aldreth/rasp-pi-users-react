jest.dontMock('../CookieAdapter');

var CookieAdapter = require('../CookieAdapter');

describe('CookieAdapter', () => {
  beforeEach(() => {
    document.cookie = 'ab=cd';
    document.cookie = 'ef=gh';
    document.cookie = '__gads=ID=d01353fc9747d9f0:T=1437377663:S=ALNI_MaB-OSFx5ZBWi_NCq86MJTzWMTa5A';
  });

  it('returns all the cookies', () => {
    var cookieAdapter = new CookieAdapter(document);
    var cookies       = cookieAdapter.all();

    expect(cookies.ab).toEqual('cd');
    expect(cookies.ef).toEqual('gh');
    expect(cookies.__gads).toEqual('ID=d01353fc9747d9f0:T=1437377663:S=ALNI_MaB-OSFx5ZBWi_NCq86MJTzWMTa5A');
  });

  it('returns cookies with multiple `=` correctly', () => {
    var cookieAdapter = new CookieAdapter(document);
    var cookies       = cookieAdapter.all();

    expect(cookies.__gads).toEqual('ID=d01353fc9747d9f0:T=1437377663:S=ALNI_MaB-OSFx5ZBWi_NCq86MJTzWMTa5A');
  });

  it('removes a cookie', () => {
    var cookieAdapter = new CookieAdapter(document);

    cookieAdapter.remove('ab');

    var cookies = cookieAdapter.all();
    expect(cookies.ab).toBe(undefined);
  });

  // Jest's mocked document can't cope with testing setting of domain or path for the cookie
  it('sets a cookie with an expiry date', () => {
    var cookieAdapter = new CookieAdapter(document);
    var name = 'mycookie';
    var value = 'myvalue';
    var date = new Date();
    date.setTime(date.getTime() + (24 * 60 * 60 * 1000));
    var options = {
      expires: date,
    };

    cookieAdapter.set(name, value, options);

    var cookies = cookieAdapter.all();
    expect(cookies.mycookie).toEqual('myvalue');
  });

  it('handles cookies without a value correctly', () => {
    var cookieAdapter = new CookieAdapter({cookie: 'empty;another=cookie'});
    var cookies       = cookieAdapter.all();

    expect(cookies.empty).toEqual(undefined);
    expect(cookies.another).toEqual('cookie');
  });
});
