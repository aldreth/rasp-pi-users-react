export default class CookieAdapter {

  constructor(context) {
    this.context = context;
  }

  all() {
    var cookies = this.context.cookie.split(';');

    var parsedCookies = {};

    cookies.forEach((cookie) => {
      var cookieArray = cookie.split('=');

      var key = cookieArray[0].trim();
      var value;

      // A cookie doesn't necessarily have a single `=` but we do expect it to
      // have 2 elements, a key (at [0]) and a value (everything else). This just
      // makes sure that we have all the value when the cookie contains multiple =
      if (cookieArray.length > 2) {
        value = cookieArray.slice(1, cookieArray.length).join('=');
      } else {
        value = cookieArray[1];
      }

      parsedCookies[key] = value ? value.trim() : undefined;
    });

    return parsedCookies;
  }

  /**
   * Client side set based on same interface as existing server side Koa
   * which uses Cookies (https://github.com/pillarjs/cookies)
   * Note that some options should not be available client side
   * TODO : options.maxAge, options.overwrite
   *
   * @param {String} name
   * @param {String} value
   * @param {Object} options = {}
   * * (not supported) `signed` sign the cookie value
   * * `expires` a Date for cookie expiration
   * * `path` cookie path, /' by default
   * * `domain` cookie domain
   * * (not supported) `secure` secure cookie
   * * (not supported) `httpOnly` server-accessible cookie, true by default
   */
  set(name, value, options = {}) {
    // If no value - expire the cookie
    if (!value) {
      this.remove(name);
      return;
    }

    const cookieExpires = options.expires ? '; expires=' + options.expires.toGMTString() : '';
    const cookieDomain = options.domain ? '; domain=' + options.domain : '';
    const cookiePath = options.path ? '; path=' + options.path : '';

    this.context.cookie = name + '=' + value + cookieExpires + cookieDomain + cookiePath;
  }

  remove(name) {
    this.context.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

}
