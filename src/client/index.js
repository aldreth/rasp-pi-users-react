import React from 'react';
import ReactDOM from 'react-dom';
import Router from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import Promise from 'bluebird';

import getConfig from '../../get-config';
import AppContext from '../shared/components/AppContext';
import routes from '../shared/routes';
import CookieJar from '../helpers/CookieJar';
import ClientCookieAdapter from './CookieAdapter';
import NotFoundException from '../shared/routing/NotFoundException';
import getStore from '../shared/getStore';
import { setFeature, setFeatures } from '../shared/actions/features';

window.Promise = Promise;

const appConfig = getConfig();
const initialState = window.STORE;
const cookieJar = new CookieJar(new ClientCookieAdapter(window.document));
const history = createBrowserHistory();
const store = getStore(initialState);

function render() {
  ReactDOM.render(
    <AppContext
      cookieJar={cookieJar}
      store={store}>
      <Router
        routes={routes}
        history={history}
        />
    </AppContext>,
    document.getElementById('app')
  );
}

window.setFeature = (key, value) => {
  if (typeof key === 'object') {
    store.dispatch(setFeatures(key));
  } else {
    store.dispatch(setFeature(key, value));
  }
};

// react-router doesn't handle external redirects
window.onerror = (msg, file, line, column, err) => {
  if (err instanceof NotFoundException) {
    window.location = 'http://www.sky.com/skycom/404';
  }
};

// persist any store changes to a cookie
store.subscribe(() => {
  const noop = () => '';
  // IE8/9 use base64.encode
  const b64encode = (typeof btoa !== 'undefined' && btoa)
    || (typeof base64 !== 'undefined' && base64.encode)
    || noop;
  const options = {
    path: '/',
    domain: '.sky.com',
  };

  const appState = store.getState();
  cookieJar.set(appConfig.stateCookie, b64encode(JSON.stringify(appState)), options);
});

render();
