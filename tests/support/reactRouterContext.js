/* eslint react/no-multi-comp:0 */
jest.dontMock('history/lib/createMemoryHistory');

// Modifed from the original at https://labs.chie.do/jest-testing-with-react-router/
var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var createHistory = require('history/lib/createMemoryHistory');

function reactRouterContext(Component, props, location = '/') {
  var Wrapper = React.createClass({
    render: function render() {
      return (
        <Component {...props} />
      );
    },
  });

  return React.createClass({
    childContextTypes: {
      features: React.PropTypes.object,
      cookieJar: React.PropTypes.object,
      store: React.PropTypes.object,
    },

    getChildContext: function getChildContext() {
      return {
        features: {
          get: () => true,
          all: () => {},
        },
        cookieJar: {
          removeSSO: () => {},
          get: () => '',
          set: () => {},
          all: () => {},
          remove: () => {},
          refresh: () => {},
        },
        store: {
          subscribe: () => {},
          dispatch: () => {},
          getState: () => {
            return Object.assign({
              features: {},
              user: {
                isTrial: true,
              },
            }, props);
          },
        },
      };
    },

    render: function render() {
      return (
        <Router history={createHistory(location)}>
          <Route path={location} component={Wrapper} />
        </Router>
      );
    },
  });
}

module.exports = reactRouterContext;
