// Jest test pre-processor, can't use ES6 here
var babel = require('babel-core');

module.exports = {
  process: function(src, filename) {
    // Ignore all files within node_modules
    // babel files can be .js, .es, .jsx or .es6
    if (filename.indexOf('node_modules') === -1 && babel.canCompile(filename)) {
      return babel.transform(src, {
        filename: filename,
        retainLines: true,
      }).code;
    }

    return src;
  },
};
