export default function getReduxStore() {
  return {
    subscribe: () => {},
    dispatch: () => {},
    getState: () => {
      return {
        features: {},
      };
    },
  };
}
