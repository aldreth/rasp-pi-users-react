module.exports = {
  // Uncomment line below to disable this test temporarily
  // '@disabled': true,

  'tags': ['404'],

  'If I visit a non-existant page, I will be sent to the 404 page': (browser) => {
    var pauseDefault = browser.globals.pauseDefault;

    return browser
      .url(browser.launch_url + '/random')
      .pause(pauseDefault)
      .url(function checkUrl(result) {
        this.assert.equal(result.value, 'http://www.sky.com/skycom/404', 'Url equals the expected 404 redirect');
      })
      .end();
  },
};
