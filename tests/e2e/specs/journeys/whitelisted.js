// TODO

module.exports = {
  // Uncomment line below to disable this test temporarily
  // '@disabled': true,

  'tags': ['whitelisted'],

  'If I visit /whitelisted directly I will be allowed to see the whitelisted page': (browser) => {
    return browser
      .url(browser.launch_url + '/whitelisted')
      .waitForElementVisible('#step-container')
      .assert.containsText('#step-container h1', 'A whitelisted page')
      .end();
  },
};
