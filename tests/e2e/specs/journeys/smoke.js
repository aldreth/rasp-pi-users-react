var fetch = require('isomorphic-fetch');

module.exports = {
  // Uncomment line below to disable this test temporarily
  //  '@disabled': true,

  'tags': ['smoke'],

  'Katana happy path': (browser) => {
    return browser
      .url(browser.launch_url)
      .waitForElementVisible('#step-container')
      .assert.containsText('#step-container h1', 'Welcome to Katana')
      .click('#step-container a.page')
      .assert.containsText('#step-container h1', 'Welcome to a page')
      .end();
  },

  'Availability check endpoint': (browser) => {
    const url = browser.launch_url + '/ping';

    fetch(url, {
      headers: {
        'Accept': 'application/json',
      },
    })
    .then(res => {
      browser.assert.equal(200, res.status);
      browser.end();
    });
  },
};
