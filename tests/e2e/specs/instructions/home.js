var formatter = require('../../../support/formatter');

module.exports = {
  'Clicking whitelisted sends the user to the whitelisted page': (browser) => {
    return browser
      .url(browser.launch_url)
      .pause(browser.globals.pauseDefault)
      .click('#step-container a.users')
      .pause(browser.globals.pauseDefault)
      .assert.containsText('#step-container h1', 'All users')
      .perform(formatter);
  },
};
