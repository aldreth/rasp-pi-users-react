var Proxy = require('browsermob-proxy').Proxy;

var appConfig = require('../../../../get-config')();

var TrafficAnalyser = require('traffic-analyser');
var skyTagsParser = new TrafficAnalyser();
var filters = {
  include: 'metrics.sky.com',
  exclude: 'callback',
};

var proxy;

module.exports = {
// Uncomment line below to diable this test temporarily
//   '@disabled': true,

  'tags': ['analytics'],

  before: () => {
    proxy = new Proxy({proxyPort: appConfig.mobProxyPort});
  },

  'Correct report suite should be set for analytics': (browser) => {
    // Set up proxy options
    var proxyOptions = {
      name: 'Analytics Page Title',
    };

    // Run through the browser steps we want to analyse
    function triggerAnalyticsEvents(proxyServer, analyserFunc) {
      browser
        .url(browser.launch_url + '/home')
        .waitForElementVisible('body')
        .waitForElementVisible('#step-container')
        .analyseTraffic(analyserFunc);
    }

    // Analyse the results from the proxy
    function analyser(err, data) {
      var metrics;
      var lastRequestUrl;
      var reportSuiteAssertion = false;
      var expectedAdobeAcct = appConfig.analyticsAcct;

      if (err) {
        console.log('ERR : ' + err);
      } else {
        metrics = JSON.parse(data);
        skyTagsParser.parseHarJson(metrics, filters);

        lastRequestUrl = skyTagsParser.lastRequestUrl();
        if (lastRequestUrl) {
          reportSuiteAssertion = (lastRequestUrl.indexOf(expectedAdobeAcct) > -1);
        }
      }

      browser.assert.equal(reportSuiteAssertion, true, 'Analytics should include the correct report suite');
      browser.end();
    }

    // Run our test through the proxy
    // this automatically halts the proxy also so no need for an afterEach clean up
    proxy.cbHAR(proxyOptions, triggerAnalyticsEvents, analyser);
  },

  'Page Title and breadcrumb should be sent to analytics on home page': (browser) => {
    // Set up proxy options
    var proxyOptions = {
      name: 'Analytics Page Title',
    };

    // Run through the browser steps we want to analyse
    function triggerAnalyticsEvents(proxyServer, analyserFunc) {
      browser
        .url(browser.launch_url + '/home')
        .waitForElementVisible('body')
        .waitForElementVisible('#step-container')
        .analyseTraffic(analyserFunc);
    }

    // Analyse the results from the proxy
    function analyser(err, data) {
      var param;
      var metrics;
      var eventAssertion = false;
      var titleAssertion = false;
      var crumbAssertion = false;
      var contentTypeAssertion = false;

      if (err) {
        console.log('ERR : ' + err);
      } else {
        metrics = JSON.parse(data);
        skyTagsParser.parseHarJson(metrics);

        param = skyTagsParser.checkLatestParam('events');
        if (param && param.value) {
          eventAssertion = (param.value.indexOf('event1') > -1);
        }

        param = skyTagsParser.checkLatestParam('pageName');
console.log(param);
        if (param && param.value) {
          titleAssertion = (param.value.indexOf('Katana | Home') > -1);
        }

        param = skyTagsParser.checkLatestParam('c59');
        if (param && param.value) {
          crumbAssertion = (param.value.indexOf('Katana:Home') > -1);
        }

        param = skyTagsParser.checkLatestParam('c20');
        if (param && param.value) {
          contentTypeAssertion = (param.value.indexOf('katana-content-type') > -1);
        }
      }

      browser.assert.equal(eventAssertion, true, 'Last analytics event should be a pageLoad event');
      browser.assert.equal(titleAssertion, true, 'Analytics sends correct page title for home page');
      browser.assert.equal(crumbAssertion, true, 'Analytics sends correct breadcrumbs for home page');
      browser.assert.equal(contentTypeAssertion, true, 'Analytics should include the correct content type');
      browser.end();
    }

    // Run our test through the proxy
    // this automatically halts the proxy also so no need for an afterEach clean up
    proxy.cbHAR(proxyOptions, triggerAnalyticsEvents, analyser);
  },

  'Page Title and breadcrumb should be sent to analytics for click through journey': (browser) => {
    // Set up proxy options
    var proxyOptions = {
      name: 'Analytics Page Title',
    };

    // Run through the browser steps we want to analyse
    function triggerAnalyticsEvents(proxyServer, analyserFunc) {
      browser
        .url(browser.launch_url + '/')
        .waitForElementVisible('body')
        .waitForElementVisible('#step-container')
        .click('.page')
        .waitForElementVisible('h1.title')
        .analyseTraffic(analyserFunc);
    }

    // Analyse the results from the proxy
    function analyser(err, data) {
      var param;
      var metrics;
      var titleAssertion = false;
      var crumbAssertion = false;
      var clickAssertion = false;

      if (err) {
        console.log('ERR : ' + err);
      } else {
        metrics = JSON.parse(data);
        skyTagsParser.parseHarJson(metrics, filters);

        // Ensure prop15 / c15 click event is sent
        param = skyTagsParser.checkLatestParam('c15');
        if (param && param.value) {
          clickAssertion = (param.value.indexOf('page') > -1);
        }

        param = skyTagsParser.checkLatestParam('pageName');
        if (param && param.value) {
          titleAssertion = (param.value.indexOf('Katana | Page') > -1);
        }

        param = skyTagsParser.checkLatestParam('c59');
        if (param && param.value) {
          crumbAssertion = (param.value.indexOf('Katana:Home:Page') > -1);
        }
      }

      browser.assert.equal(clickAssertion, true, 'Analytics should record click events');
      browser.assert.equal(titleAssertion, true, 'Analytics should send updated page title for click through journey');
      browser.assert.equal(crumbAssertion, true, 'Analytics should send updated breadcrumbs for click through journey');
      browser.end();
    }

    // Run our test through the proxy
    // this automatically halts the proxy also so no need for an afterEach clean up
    proxy.cbHAR(proxyOptions, triggerAnalyticsEvents, analyser);
  },
};
