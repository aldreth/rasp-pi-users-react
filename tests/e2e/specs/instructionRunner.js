var requireDir = require('require-dir');

var Humanise = require('../../../build/helpers/Humanise');
var mockMe = require('../mock-me');
var unMockMe = require('../un-mock-me');
var mocks = require('../mocks/instructions');
var appConfig = require('../../../get-config')();

/*
 * A test runner for the more "unit" based integration tests
 * this enables a very quick run though of the individual flow steps so that they can simply check that they send
 * onwards to the correct next step without the need for any setup/state
 */
module.exports = {
  // Uncomment line below to diable this test temporarily
  // "@disabled": true,

  tags: ['instructions'],

  before: (browser) => {
    mockMe(mocks);
    // Visit home page to set JWT token to allow browsing of the site
    browser
      .url(browser.launch_url + '/')
      .setCookie({
        name: 'skySSO',
        value: 'important',
      })
      .setCookie({
        name: 'FEATURE_isTrialEnabled',
        value: 'true',
      })
      .setCookie({
        name: appConfig.stateCookie,
        value: 'DELETE_ME',
        path: '/',
        domain: '.sky.com',
      })
      .refresh()
      .waitForElementVisible('body')
      .waitForElementVisible('#step-container')
    ;
  },

  after: (browser) => {
    browser.end();
    unMockMe();
  },

  'Run all instruction tests': (browser) => {
    var filesToTest = requireDir('./instructions');

    console.log(' ');
    console.log('========================================');

    Object.getOwnPropertyNames(filesToTest).forEach((key) => {
      var testFile = filesToTest[key];

      Object.getOwnPropertyNames(testFile).forEach((method) => {
        var title = 'Katana | ' + Humanise.parse(key);

        browser
          .perform((client, done) => {
            console.log(key, ':', method);
            console.log('----------------------------------------');
            done();
          })
          .url(browser.launch_url + '/' + key)
          .waitForElementVisible('body')
          .waitForElementVisible('#step-container')
          .assert.title(title)
          .perform((client, done) => {
            console.log('----------------------------------------');
            done();
          });

        testFile[method](browser, method);
      });
    });
  },
};
