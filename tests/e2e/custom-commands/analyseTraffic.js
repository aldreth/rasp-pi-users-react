function analyseTraffic() {}

analyseTraffic.prototype.command = function(analyser) { // Do not fat arrow this
  if (!analyser) {
    return this;
  }

  return analyser();
};

module.exports = analyseTraffic;
