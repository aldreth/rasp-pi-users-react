var appConfig = require('../../get-config')();
var fetch = require('isomorphic-fetch');

module.exports = function() {
  // Delete the imposter service
  fetch('http://127.0.0.1:2525/imposters/' + appConfig.mockingPort, {
    method: 'delete',
  });
};
