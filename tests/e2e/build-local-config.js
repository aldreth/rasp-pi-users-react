import fs from 'fs';

import getConfig from '../../get-config';

var appConfig = getConfig();

var outputFilename = './tests/e2e/nightwatch.local.json';

var nwConfig = {
  'src_folders': ['tests/e2e/specs'],
  'output_folder': 'reports',
  'custom_commands_path': [
    'tests/e2e/custom-commands',
  ],
  'custom_assertions_path': '',
  'page_objects_path': '',
  'globals_path': './tests/e2e/globals.js',

  'selenium': {
    'start_process': true,
    'server_path': './bin/selenium-server-standalone-2.48.2.jar',
    'log_path': '',
    'host': '127.0.0.1',
    'port': 4444,
    'cli_args': {
      'webdriver.chrome.driver': '',
      'webdriver.ie.driver': '',
    },
  },

  'test_settings': {
    'default': {
      'launch_url': 'http://localhost:' + appConfig.port || 3010,
      'selenium_port': 4444,
      'selenium_host': 'localhost',
      'silent': true,
      'screenshots': {
        'enabled': false,
        'path': '',
      },
      'exclude': [
        'instructions/*.js',
      ],
    },

    'proxy': {
      'desiredCapabilities': {
        'browserName': 'firefox',
        'javascriptEnabled': true,
        'acceptSslCerts': true,
        'proxy': {
          'proxyType': 'manual',
          'httpProxy': 'localhost:' + appConfig.mobProxyPort,
        },
      },
    },

    'chrome': {
      'desiredCapabilities': {
        'browserName': 'chrome',
        'javascriptEnabled': true,
        'acceptSslCerts': true,
      },
    },

    'stage': {
      'launch_url': 'http://stage-katana.herokuapp.com',
      'desiredCapabilities': {
        'javascriptEnabled': true,
        'acceptSslCerts': true,
      },
    },

    'test': {
      'launch_url': 'http://test-katana.herokuapp.com',
      'desiredCapabilities': {
        'javascriptEnabled': true,
        'acceptSslCerts': true,
      },
    },

    'pre-prod': {
      'launch_url': 'http://pre-prod-katana.herokuapp.com',
      'desiredCapabilities': {
        'javascriptEnabled': true,
        'acceptSslCerts': true,
      },
    },

    'production': {
      'launch_url': 'http://katana.herokuapp.com',
      'desiredCapabilities': {
        'javascriptEnabled': true,
        'acceptSslCerts': true,
      },
    },
  },
};

export default function() {
  fs.writeFile(
    outputFilename,
    JSON.stringify(nwConfig, null, 4),
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log('JSON saved to ' + outputFilename);
      }
    }
  );
}
