import fs from 'fs';

import getConfig from '../../get-config';

var appConfig = getConfig();

var outputFilename = './tests/e2e/nightwatch.json';

var desiredCapabilities = {
  javascriptEnabled: true,
  acceptSslCerts: true,
  'browserstack.user': appConfig.bsUser,
  'browserstack.key': appConfig.bsAccessKey,
  'browserstack.local': true,
  'browserstack.tunnelIdentifier': appConfig.crossBrowserTunnel,
  'browserstack.debug': true,
};

// Circle Node Indexes start from 0
// Circle Parallelism Needs to be increased in project settings for these to take effect
var index = Number(appConfig.thisCircleNode);
switch (index) {
case 0:
  desiredCapabilities.platform = 'WINDOWS';
  desiredCapabilities.browserName = 'chrome';
  desiredCapabilities.os = 'WINDOWS';
  desiredCapabilities.os_version = '10';
  break;

// Need a ticket to decide what other devices to test when we up parallelism

// case 1:
//   desiredCapabilities.platform = 'WINDOWS';
//   desiredCapabilities.browserName = 'chrome';
//   desiredCapabilities.os = 'WINDOWS';
//   desiredCapabilities.os_version = '10';
//   break;

// case 2:
//   desiredCapabilities.platform = 'WINDOWS';
//   desiredCapabilities.browserName = 'chrome';
//   desiredCapabilities.os = 'WINDOWS';
//   desiredCapabilities.os_version = '10';
//   break;

// case 3:
//   desiredCapabilities.platform = 'WINDOWS';
//   desiredCapabilities.browserName = 'chrome';
//   desiredCapabilities.os = 'WINDOWS';
//   desiredCapabilities.os_version = '10';
//   break;

default:
  desiredCapabilities.platform = 'WINDOWS';
  desiredCapabilities.browserName = 'chrome';
  desiredCapabilities.os = 'WINDOWS';
  desiredCapabilities.os_version = '10';
}

desiredCapabilities.name = desiredCapabilities.platform + ' | ' +
                           desiredCapabilities.browserName + ' | ' +
                           desiredCapabilities.os_version;

console.log('Tests for : ' + desiredCapabilities.name +
            ' Tunnel : ' + desiredCapabilities.tunnelIdentifier);

var nwConfig = {
  'src_folders': ['tests/e2e/specs'],
  'globals_path': './tests/e2e/globals.js',

  'selenium': {
    'start_process': false,
    'host': 'hub.browserstack.com',
    'port': 80,
  },

  'test_settings': {
    'default': {
      'launch_url': 'http://local-katana.sky.com:' + appConfig.port || 3010,
      'selenium_port': 80,
      'selenium_host': 'hub.browserstack.com',
      'silent': true,
      'screenshots': {
        'enabled': false,
        'path': '',
      },
      'exclude': [
        'instructions/*.js',
      ],
      'desiredCapabilities': desiredCapabilities,
    },
  },
};

export default function() {
  fs.writeFile(
    outputFilename,
    JSON.stringify(nwConfig, null, 4),
    (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log('JSON saved to ' + outputFilename);
      }
    }
  );
}
