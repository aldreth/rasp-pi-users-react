var appConfig = require('../../../get-config')();

module.exports = {
  'port': appConfig.mockingPort,
  'protocol': 'http',
  'stubs': [{
    'predicates': [
      {
        'startsWith': {
          'path': '/widebandenabled/',
        },
      },
    ],
    'responses': [
      { 'is': {
        'statusCode': 200,
        'headers': {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
        'body': '{"isWidebandEnabled":true,"isWidebandPending":false}',
      }},
    ],
  }],
};
