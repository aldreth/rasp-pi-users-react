var appConfig = require('../../../get-config')();

module.exports = {
  'port': appConfig.mockingPort,
  'protocol': 'http',
  'stubs': [{
    'predicates': [{
      'startsWith': {
        'path': '/widebandenabled/',
      },
    }],
    'responses': [
      // Trial Feature turned on, Trial Enabled Customer goes to trial page
      {
        'is': {
          'statusCode': 200,
          'headers': {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
          'body': '{"isWidebandEnabled":true,"isWidebandPending":false}',
        },
      },
      {
        'is': {
          'statusCode': 200,
          'headers': {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
          'body': '{"isWidebandEnabled":true,"isWidebandPending":false}',
        },
      },

      // Trial Feature turned on, Trial Enabled Customer goes to trial page and refresh it
      {
        'is': {
          'statusCode': 200,
          'headers': {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
          'body': '{"isWidebandEnabled":true,"isWidebandPending":false}',
        },
      },


      // Trial Feature turned on, Trial Pending variable link goes to standard page
      {
        'is': {
          'statusCode': 200,
          'headers': {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
          'body': '{"isWidebandEnabled":false,"isWidebandPending":true}',
        },
      },

      // Trial Feature turned on, Customer Not Signed in goes to standard page
      {
        'is': {
          'statusCode': 401,
          'headers': {
            'Content-Type': 'text/html',
          },
          'body': 'This request requires HTTP authentication',
        },
      },

      // Trial Feature turned off, trial autorised customer redirected to home
    ],
  }],
};
