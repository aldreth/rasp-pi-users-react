import fs from 'fs';

import getConfig from '../../get-config';

var appConfig = getConfig();

var outputFilename = './tests/e2e/nightwatch.json';

var desiredCapabilities = {
  javascriptEnabled: true,
  acceptSslCerts: true,
  tunnelIdentifier: appConfig.crossBrowserTunnel,
};

// Circle Node Indexes start from 0
// Circle Parallelism Needs to be increased in project settings for these to take effect
var index = Number(appConfig.thisCircleNode);
switch (index) {
case 0:
  desiredCapabilities.platform = 'Windows 10';
  desiredCapabilities.browserName = 'chrome';
  desiredCapabilities.browserVersion = '46.0';
  break;

// Need a ticket to decide what other devices to test when we up parallelism

// case 1:
//   desiredCapabilities.platform = 'Windows 10';
//   desiredCapabilities.browserName = 'chrome';
//   desiredCapabilities.browserVersion = '46.0';
//   break;

// case 2:
//   desiredCapabilities.platform = 'Windows 10';
//   desiredCapabilities.browserName = 'chrome';
//   desiredCapabilities.browserVersion = '46.0';
//   break;

// case 3:
//   desiredCapabilities.platform = 'Windows 10';
//   desiredCapabilities.browserName = 'chrome';
//   desiredCapabilities.browserVersion = '46.0';
//   break;

default:
  desiredCapabilities.platform = 'Windows 10';
  desiredCapabilities.browserName = 'chrome';
  desiredCapabilities.browserVersion = '46.0';
}
desiredCapabilities.name = desiredCapabilities.platform + ' | ' +
                           desiredCapabilities.browserName + ' | ' +
                           desiredCapabilities.browserVersion;

console.log('Tests for : ' + desiredCapabilities.name +
            ' Tunnel : ' + desiredCapabilities.tunnelIdentifier);

var nwConfig = {
  'src_folders': ['tests/e2e/specs'],
  'globals_path': './tests/e2e/globals.js',

  'selenium': {
    'start_process': false,
    'host': 'ondemand.saucelabs.com',
    'port': 80,
  },

  'test_settings': {
    'default': {
      'launch_url': 'http://local-katana.sky.com:' + appConfig.port || 3010,
      'selenium_port': 80,
      'selenium_host': 'ondemand.saucelabs.com',
      'silent': true,
      'username': appConfig.scUser,
      'access_key': appConfig.scAccessKey,
      'screenshots': {
        'enabled': false,
        'path': '',
      },
      'exclude': [
        'instructions/*.js',
      ],
      'desiredCapabilities': desiredCapabilities,
    },
  },
};

export default function() {
  fs.writeFile(
    outputFilename,
    JSON.stringify(nwConfig, null, 4),
    (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log('JSON saved to ' + outputFilename);
      }
    }
  );
}
