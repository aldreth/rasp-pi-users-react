var fetch = require('isomorphic-fetch');
var unMockMe = require('./un-mock-me');

var fetchMiddleware = require('../../build/helpers/fetch');
var status = fetchMiddleware.status;
var json = fetchMiddleware.json;

module.exports = function(mocks) {
  // Delete any existing mountebank mocks
  unMockMe();

  // Stub the service calls
  fetch('http://127.0.0.1:2525/imposters/', {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify(mocks),
  })
  .then(status)
  .then(json)
  .catch(err => {
    throw err;
  });
};
