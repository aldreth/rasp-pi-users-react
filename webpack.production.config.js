var webpack = require('webpack');
var DefinePlugin = webpack.DefinePlugin;
var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var webpackConfig = {
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'source-map-loader!transform?envify',
      },
    ],
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('../css/styles.css'),
    new DefinePlugin({
      IS_CLIENT: true,
    }),
    new UglifyJsPlugin({
      sourceMap: false,
      compress: {
        warnings: false,
      },
    }),
  ],
  entry: [
    './build/client/index.js',
    './scss/index.scss',
  ],
  output: {
    path: './public/assets/js',
    filename: 'bundle.js',
  },
  devtool: 'inline-source-map',
  cache: true,
};

module.exports = webpackConfig;
