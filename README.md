## a react front end to view users

Intro:
-----------
This is a react front end to a rails api. It is built on a template application developed by my team in sky, there is still sky specific code remaining, it could be cleaned up more!

It is currently hosted on [heroku](http://rasp-pi-users-react.herokuapp.com) - it is on a free dyno, so will take a little while to start on first hit. 

Only viewing all users and individual users is implemented so far. Files I have added for this are:

* `src/helpers/user.js` - functions to get users from the api
* `src/helpers/__tests__/user.js` - tests for the functions to get users from the api
* `src/shared/actions/user.js` - redux actions for getting users
* `src/shared/actions/__tests__/user.js` - tests for the redux actions for getting users
* `src/shared/reducers/user.js` - redux reducers for getting users
* `src/shared/reducers/__tests__/user.js` - tests for the redux reducers for getting users
* `src/shared/components/users.jsx` - a template to show all users
* `src/shared/components/user-link.jsx` - a template used by the above template
* `src/shared/components/user.jsx` - a template used to show a single user-link

All the unit tests are fixed, they can be run with the command `gulp jest`. The integration tests are still broken after I removed a large amount of sky set up.



------------
* [Requirements](#requirements)
* [Installation](#install)
* [Run App](#run-app)
* [Releasing a new Katana version](#releasing-a-new-katana-version)
* [Architecture](#architecture)

Further reading:
* [Testing](docs/testing.md)
* [Feature Switches](docs/featureSwitches.md)
* [Multi-Variant/A-B Testing](docs/mvt.md)
* [Responsive Images](docs/responsiveImages.md)
* [Application State](docs/applicationState.md)
* [Instruction Steps](docs/instructions.md)
* [Protected Routes](docs/protectedRoutes.md)


Requirements:
------------

* Recommend nvm (node version manager) for ease of install of `node 4.2.6`
* `npm install -g gulp`
* Make sure JAVA_HOME is set correctly via `export JAVA_HOME=$(/usr/libexec/java_home)`

___

Install
-------

* In development establish environment variables by copying `.env.dist` to `.env` file
* `npm install`
  * npm installs are controlled by the `npm-shrinkwrap.json` which monitors allowed package versions
  * if making changes to `package.json` in the future, you must update the shrinkwrap with the following command
  * `npm shrinkwrap`
* Add `127.0.0.1 local-katana.sky.com` to your hosts file
* Copy `.env.dist` to `.env` and configure to your needs

___

Run App
-------

* `gulp` Default port 4000 - configurable via .env file
* Navigate to http://local-katana.sky.com:4000
* Mountebank is run automatically by the above command for mocking services
  * Navigate to localhost:2525 to manage these mocks
  * localhost:4001 - default port will return Trial customer
* To test Classic customer paths use the following command to start the server
  * `CLASSIC=true gulp`

___

Releasing a new Katana version
------------------------------

Once your feature branch has been merged, switch to master:

```bash
git checkout master
```

Depending on the release type, run one of the following commands:

```bash
gulp release:patch
gulp release:minor
gulp release:major

# then push

git push origin master
git push origin --tags
```

This will bump the Katana version, create a commit, and tag the current release. Tags are automatically being picked up by Github under https://github.com/sky-uk/katana/releases.

___

Architecture
------------
Katana is an [isomorphic](http://isomorphic.net/) app that makes use of the [react](http://facebook.github.io/react/) library served over [koa](http://koajs.com/)

Unit tests are driven by [jest](http://facebook.github.io/jest/) and [react's test utilities](https://facebook.github.io/react/docs/test-utils.html).

Integration tests via: [nightwatch](http://nightwatchjs.org/)

---

Structure
---------

```bash
.
├── src                                     # Main source code
│   ├── client                              # Client-side code
│   │   └── index.js
│   ├── server                              # Service-side code
│   │   ├── index.js
│   │   ├── middleware                      # Koa middleware
│   ├── shared                              # Universal ("isomorphic") code
│   │   ├── app.jsx                         # The root React component
│   │   ├── routes.jsx                      # Routing definitions
│   │   ├── components                      # React components live here
│   │   │   ├── AppContext.jsx              # This injects Katana-specific context into `../app.jsx`
│   │   │   └── instructions                # Anything in here will be auto-loaded and served
│   │   ├── actions                         # Redux actions and action creators
│   │   └── reducers                        # Redux reducers
│   └── helpers                             # Any helper functions
│
├── views                                   # Static view files (Handlebars)
│   ├── index.hbs                           # The main entry point
│   └── partials                            # The masthead lives here
│
├── public                                  # The public folder. Anything in here will be served.
│   └── assets
│       ├── css                             # The compiled output from `/scss`
│       ├── img                             # The compiled output from `gulp resize-images`
│       └── js
│           └── bundle.js                   # The compiled output from `/src/client/index.js`
│
├── scss
│   ├── index.scss                          # The main SCSS entry point
│   ├── mixins                              # SCSS mixins
│   └── modules                             # Page-specific SCSS
│
├── tests                                   # Anything testing-related
│   ├── e2e
│   │   ├── build-bs-config.js              # Nightwatch Browserstack config
│   │   ├── build-local-config.js           # Nightwatch local config
│   │   ├── build-sc-config.js              # Nightwatch SauceLabs config
│   │   ├── mocks                           # Mountebank mock definitions
│   │   ├── specs                           # Integration tests
│   │   │   ├── instructionRunner.js        # Runs all tests under /instructions
│   │   │   ├── instructions
│   └── support                             # Testing helpers
│
├── tasks                                   # Gulp tasks
├── docs                                    # Documentation lives here
├── bin                                     # Binary files, e.g. Browserstack / SauceLabs
├── build                                   # This is where the transpiled files from `/src` get put
├── original-images                         # `gulp resize-images` looks for images here
├── reports                                 # Test reports are being written here
│
├── get-config.js                           # One configuration to rule them all
├── gulpfile.js                             # Entry point for all gulp tasks
├── Procfile                                # Heroku's run file
├── circle.yml.dist                         # Copy to `circle.yml`
├── newrelic.js                             # Generic NewRelic setup
├── npm-shrinkwrap.json                     # Used by `npm install`. Update with `npm shrinkwrap`.
├── webpack.config.js                       # Webpack config (development)
└── webpack.production.config.js            # Webpack config (production)
```
