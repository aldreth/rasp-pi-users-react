import gulp from 'gulp';
import sauceConnectLauncher from 'sauce-connect-launcher';
import getConfig from '../get-config';
var appConfig = getConfig();

let sauceConnection;

gulp.task('launch:sauce-labs', done => {
  sauceConnectLauncher(
    {
      username: appConfig.scUser,
      accessKey: appConfig.scAccessKey,
      tunnelIdentifier: appConfig.crossBrowserTunnel,
    },
    function launch(err, sauceConnectProcess) {
      if (err) {
        console.error(err.message);
        return;
      }
      console.log('Sauce Connect ready');

      sauceConnection = sauceConnectProcess;
      done();
    }
  );
});

gulp.task('close:sauce-labs', ['nw:runsc'], done => {
  sauceConnection.close(function close() {
    console.log('Closed Sauce Connect process');
    done();
  });
});
