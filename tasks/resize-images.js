/**
 * Resize images to use in the Picture responsive image component
 * It will resize all images in the original-images directory & directories within it
 * It ouputs 3 files; for phone, tablet & desktop; in the public/assets/img directory,
 * preserving directory structure.
 * Images are resized, compressed, renamed and saved into the appropriate place.
 */

import gulp from 'gulp';
import imageResize from 'gulp-image-resize';
import rename from 'gulp-rename';
import imagemin from 'gulp-imagemin';

const resizeImages = function resize(options) {
  gulp
    .src('original-images/**/*.{jpg,png}')
    .pipe(imageResize({ width: options.width }))
    .pipe(imagemin())
    .pipe(rename((path) => { path.basename += options.fileSuffix; }))
    .pipe(gulp.dest('public/assets/img'));
};

gulp.task('resize-images', () => {
  const desktopImageResizeOptions = {
    width: 721,
    fileSuffix: '-desktop',
  };

  const tabletImageResizeOptions = {
    width: 356,
    fileSuffix: '-tablet',
  };
  const phoneImageResizeOptions = {
    width: 291,
    fileSuffix: '-phone',
  };

  resizeImages(desktopImageResizeOptions);
  resizeImages(tabletImageResizeOptions);
  resizeImages(phoneImageResizeOptions);
});
