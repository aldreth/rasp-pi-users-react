import gulp from 'gulp';
import nightwatch from 'gulp-nightwatch';
import gutil from 'gulp-util';

// Runs a local set of smoke tests
gulp.task('nw:smoke', ['nw:local-config'], function(done) {
  var tags = gutil.env.tags || '';
  var env  = gutil.env.env  || 'default';
  var skip = gutil.env.skip || '';

  gulp.src('')
    .pipe(nightwatch({
      configFile: './tests/e2e/nightwatch.local.json',
      cliArgs: {
        tag: tags,
        env: env,
        skiptags: skip,
      },
    }))
    .once('end', function() {
      done();
    });
});
