import gulp from 'gulp';
import jest from 'jest-cli';
import path from 'path';

// `gulp jest` - runs all the jest tests
gulp.task('jest', done => {
  jest.runCLI({
    config: {
      verbose: true,
      rootDir: 'src',
      testRunner: '<rootDir>/../node_modules/jest-cli/src/testRunners/jasmine/jasmine2',
      scriptPreprocessor: path.resolve(__dirname, '../tests/support', 'preprocessor.js'),
      unmockedModulePathPatterns: [
        '../node_modules/babel-runtime',
        '../node_modules/react',
        '../node_modules/react-dom',
        '../node_modules/react-addons-test-utils',
        '../node_modules/react-router',
        '../node_modules/react-redux',
        '../node_modules/history/lib/createMemoryHistory',
        '../node_modules/object-assign',
        '../node_modules/bluebird',
        '../node_modules/lodash',
        '../tests/support/reactRouterContext',
      ],
      testDirectoryName: '__tests__',
      testPathIgnorePatterns: [
        'node_modules',
        'spec/support',
      ],
      moduleFileExtensions: [
        'js',
        'json',
        'react',
        'jsx',
      ],
      globals: {
        'IS_CLIENT': false,
      },
    },
  }, __dirname, noErrors => {
    if (!noErrors) {
      process.exitCode = 1;
    }

    done();
  });
});
