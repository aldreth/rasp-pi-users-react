import gulp from 'gulp';
import fetch from 'isomorphic-fetch';
import shell from 'gulp-shell';

import { status, json } from '../src/helpers/fetch';
import getConfig from '../get-config';
var appConfig = getConfig();

gulp.task('mb:start', ['mb:stop'], shell.task([
  // sleeping because mb needs to be completely up before we can continue
  './mb && sleep 1',
]));

gulp.task('mb:stop', shell.task([
  './mbkill && sleep 1',
]));

gulp.task('mb:create-imposters', ['mb:start'], () => {
  var type = {
    'port': appConfig.mockingPort,
    'protocol': 'http',
    'stubs': [
      {
        'predicates': [
          {
            'startsWith': {
              'path': '/widebandenabled/',
            },
          },
        ],
        'responses': [{
          'is': {
            'statusCode': 200,
            'headers': {
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            },
            'body': '{"isWidebandEnabled":true,"isWidebandPending":false}',
          },
        }],
      },
      {
        'predicates': [
          {
            'startsWith': {
              'path': '/customer-info',
            },
          },
        ],
        'responses': [{
          'is': {
            'statusCode': 200,
            'headers': {
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            },
            'body': '{"title":"Mr","first_name":"Jim","last_name":"Coffey","account_numbers":["622484246401"],"authorised":true}',
          },
        }],
      },
    ],
  };

  // By default the server will treat you as a trial customer using the above mock
  // pass in a CLASSIC environment variable to examine classic path
  if (appConfig.classic) {
    type.stubs[0].responses[0].is.body = '{"isWidebandEnabled":false,"isWidebandPending":false}';
  } else if (appConfig.pending) {
    type.stubs[0].responses[0].is.body = '{"isWidebandEnabled":false,"isWidebandPending":true}';
  }

  function initiateMocks() {
    return fetch('http://127.0.0.1:2525/imposters/', {
      method: 'post',
      body: JSON.stringify(type),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    })
    .then(status)
    .then(json)
    .catch(error => {
      console.log('Mountebank request failed', error);
    });
  }

  // Once any old mocks are deleted, then initiate the new mocks
  fetch('http://127.0.0.1:2525/imposters/' + appConfig.mockingPort, {
    method: 'delete'
  })
  .then(status)
  .then(json)
  .catch(error => {
    console.log('Mountebank request failed', error);
  })
  .then(initiateMocks);
});
