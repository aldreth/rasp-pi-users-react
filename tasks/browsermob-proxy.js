import gulp from 'gulp';
import shell from 'gulp-shell';

gulp.task('browsermob:start', shell.task([
  // sleeping because mb needs to be completely up before we can continue
  './browsermob && sleep 1',
]));

gulp.task('browsermob:stop', shell.task([
  'pkill -f browsermob',
]));
