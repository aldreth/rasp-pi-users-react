import gulp from 'gulp';
import nightwatch from 'gulp-nightwatch';
import shell from 'gulp-shell';
import gutil from 'gulp-util';

// Creates the local nightwatch config
gulp.task('nw:local-config', function() {
  process.env.NODE_ENV = 'devtest';
  require('../tests/e2e/build-local-config')();
});

// `gulp nw:local` kicks off nightwatch integration tests against a local server
gulp.task('nw:local', ['nw:runlocal'], shell.task([
  './browsermobkill',
  './mbkill',
], {
  ignoreErrors: true,
}));

// Actually runs the nightwatch integration tests against a local server that is backed by browsermob for mocking of endpoints
gulp.task('nw:runlocal', ['nw:local-config', 'serve:test', 'browsermob:start'], function(done) {
  var tags = gutil.env.tags || '';
  var env  = gutil.env.env  || 'default';
  var skip = gutil.env.skip || '';

  gulp.src('')
    .pipe(nightwatch({
      configFile: './tests/e2e/nightwatch.local.json',
      cliArgs: {
        tag: tags,
        env: env,
        skiptags: skip,
      },
    }))
    .once('end', function() {
      global.testServer.shutdown();
      done();
    });
});
