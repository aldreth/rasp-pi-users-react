import gulp from 'gulp';
import git from 'gulp-git';
import semver from 'semver';
import fs from 'fs';
import path from 'path';
import pkg from '../package.json';

function writePackage(version) {
  pkg.version = version;
  pkg.katana.version = version;

  // write to package.json
  fs.writeFileSync(path.resolve(__dirname, '../package.json'), JSON.stringify(pkg, null, 2));
}

/**
 * @param  {String}   type One of `patch`, `minor` or `major`
 * @param  {Function} done
 */
function release(type, done) {
  // get current version
  const version = semver.inc(pkg.version, type);
  const versionName = 'v' + version;

  writePackage(version);

  gulp.src('./package.json')
    .pipe(git.add())
    .pipe(git.commit('༼ つ ◕_◕ ༽つ Release ' + versionName))
    .once('end', () => {
      return git.tag(versionName, 'Release ' + versionName, () => {
        done();
      });
    })
  ;
}

gulp.task('release:patch', ['release:check-unstaged'], done => {
  release('patch', done);
});

gulp.task('release:minor', ['release:check-unstaged'], done => {
  release('minor', done);
});

gulp.task('release:major', ['release:check-unstaged'], done => {
  release('major', done);
});

gulp.task('release:check-unstaged', done => {
  git.status({args: '--porcelain'}, (err, stdout) => {
    if (err) return done(err);
    if (!!stdout) return done('Uncommitted code in working directory.');

    return done();
  });
});
