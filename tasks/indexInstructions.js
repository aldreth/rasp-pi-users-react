var gulp = require('gulp');
var gutil = require('gulp-util');
var fs = require('fs');
var path = require('path');

var instructionDir = path.resolve(__dirname, '../src/shared/components/instructions');
var fileName  = path.resolve(instructionDir, 'index.js');

/**
* This task creates an index of all instructions and exports via an ES6 module (index.js)
* The purpose of this is to avoid performance penalties occured from doing dynamic `require`(s)
*/
gulp.task('index:instructions', done => {
  var indexing = new Promise((resolve, reject) => {
    fs.readdir(instructionDir, (err, files) => {
      if (err) {
        reject(err);
      }

      var count = 0;

      if (fs.existsSync(fileName)) {
        fs.unlinkSync(fileName);
      }

      var index = fs.createWriteStream(fileName);

      index.on('finish', () => {
        resolve();
      });

      index.on('error', indexErr => {
        reject(indexErr);
      });

      files.forEach(file => {
        if (path.extname(file) === '.jsx') {
          index.write('import module' + ++count + ' from \'./' + path.basename(file, '.jsx') + '\';\n');
        }
      });

      index.write('\n');
      index.write('export default {\n');

      count = 0;
      files.forEach(file => {
        if (path.extname(file) === '.jsx') {
          index.write('  \'' + path.basename(file, '.jsx') + '\': module' + ++count + ',\n');
        }
      });

      index.write('};\n');
      index.end();
    });
  });

  indexing
    .then(() => {
      gutil.log('[index:instructions]', 'Done');
      done();
    })
    .catch(error => {
      done(error);
    });
});
