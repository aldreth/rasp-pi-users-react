import gulp from 'gulp';
import nightwatch from 'gulp-nightwatch';
import shell from 'gulp-shell';
import gutil from 'gulp-util';

// Creates the browserstack config for nightwatch
gulp.task('nw:bs-config', function() {
  process.env.NODE_ENV = 'devtest';
  require('../tests/e2e/build-bs-config')();
});

// `gulp nw:bs` kicks off nightwatch integration tests against browserstack
gulp.task('nw:bs', ['nw:runbs'], shell.task([
  './browsermobkill',
  './mbkill',
], {
  ignoreErrors: true,
}));

// Actually runs the nightwatch integration tests using browserstack
gulp.task('nw:runbs', ['nw:bs-config', 'serve:test', 'browsermob:start'], function(done) {
  var tags = gutil.env.tags || '';
  var env  = gutil.env.env  || 'default';
  var skip = gutil.env.skip || '';

  gulp
    .src('')
    .pipe(nightwatch({
      configFile: './tests/e2e/nightwatch.json',
      cliArgs: {
        tag: tags,
        env: env,
        skiptags: skip,
      },
    }))
    .once('end', function() {
      global.testServer.shutdown();
      done();
    });
});
