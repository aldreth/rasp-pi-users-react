var _ = require('lodash');
var addTrailingSlashes = require('./src/helpers/addTrailingSlashes');

var serverPortOffset  = 10;
var mockingPortOffset = 1;

var getConfig = function getFunction() {
  var portOffset = process.env.NODE_ENV === 'devtest' ? serverPortOffset : 0;
  var port = (+process.env.PORT || 3000) + portOffset;
  var mcsEndpoint = process.env.MCS_ENDPOINT;
  var mockingPort = '';
  var analyticsAcct   = 'account';

  var trialSteps = addTrailingSlashes([
    '/trial',
  ]);

  var legacyDeepLinks = addTrailingSlashes([
    '/legacy-deep-link',
  ]);

  var whitelistedPaths = addTrailingSlashes([
    '',
    '/home',
    '/whitelisted',
  ]);


  // NB: these cannot be assigned to variables as otherwise uglify won't strip these blocks out
  if (process.env.NODE_ENV === 'devtest' || process.env.NODE_ENV === 'development') {
    mockingPort  = port + mockingPortOffset;
    mcsEndpoint += ':' + mockingPort + '/widebandenabled/';
  }

  if (process.env.NODE_ENV === 'production') {
    analyticsAcct = 'account';
  }

  // Client side variables
  var config = {
    analyticsAcct: analyticsAcct,
    stateCookie: 'sky-katana-appState',
    apiUrl: process.env.API_URL,
  };

  // Variables set below this line are only available on the server
  if (!(typeof IS_CLIENT !== 'undefined' ? IS_CLIENT : false)) {
    var mobServerPort = 8080;
    var mobProxyPort = 8082;
    var crossBrowserTunnel = process.env.USER;

    // parallelise circleCI containers
    if (process.env.CIRCLE_SHA1 && process.env.CIRCLE_BUILD_NUM) {
      crossBrowserTunnel = process.env.CIRCLE_SHA1 + '.' + process.env.CIRCLE_BUILD_NUM;
      if (process.env.CIRCLE_NODE_INDEX) {
        crossBrowserTunnel += '.' + process.env.CIRCLE_NODE_INDEX;
      }
    }

    _.assign(config, {
      viewsPath: 'views',
      partialsPath: 'views/partials',
      assetsUrl: process.env.ASSETS_URL,
      port: port,
      mockingPort: mockingPort,
      mobServerPort: mobServerPort,
      mobProxyPort: mobProxyPort,
      mcsEndpoint: mcsEndpoint,
      mcsConsumerKey: process.env.MCS_CONSUMER_KEY || '',
      classic: process.env.CLASSIC,
      bsUser: process.env.BROWSER_STACK_USER,
      bsAccessKey: process.env.BROWSER_STACK_ACCESS_KEY,
      scUser: process.env.SAUCE_USERNAME,
      scAccessKey: process.env.SAUCE_ACCESS_KEY,
      crossBrowserTunnel: crossBrowserTunnel,
      maxCircleNodes: process.env.CIRCLE_NODE_TOTAL,
      thisCircleNode: process.env.CIRCLE_NODE_INDEX,
      jwtSecret: process.env.JWT_SECRET,
      trialSteps: trialSteps,
      legacyDeepLinks: legacyDeepLinks,
      whitelistedPaths: whitelistedPaths,
      googleAnalyticsId: process.env.GOOGLE_ANALYTICS_ID,
      features: {
        isTrialEnabled: process.env.FEATURE_isTrialEnabled || false,
        isTargetEnabled: process.env.FEATURE_isTargetEnabled || false,
      },
      insightUrl: process.env.NEWRELIC_INSIGHT_URL,
      insightKey: process.env.NEWRELIC_INSIGHT_KEY,
    });
  }

  return config;
};

module.exports = getConfig;
