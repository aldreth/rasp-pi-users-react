var DefinePlugin = require('webpack').DefinePlugin;
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');

var webpackConfig = {
  watch: true,
  resolve: {
    extensions: ['', '.js', '.jsx'],
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'source-map-loader!transform/cacheable?envify',
      },
    ],
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader'),
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel',
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('../css/styles.css'),
    new DefinePlugin({
      IS_CLIENT: true,
    }),
    new LiveReloadPlugin({appendScriptTag: true}),
  ],
  entry: [
    './src/client/index.js',
    './scss/index.scss',
  ],
  output: {
    path: './public/assets/js',
    filename: 'bundle.js',
  },
  devtool: 'inline-source-map',
  cache: true,
};

module.exports = webpackConfig;
