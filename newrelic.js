var env = process.env.NODE_ENV;

exports.config = {
  logging: {
    level: 'info',
  },
  app_name: [process.env.NODE_ENV + '-katana'],
  agent_enabled: env === 'development' ? false : true,
};
