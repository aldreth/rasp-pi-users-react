# Protected Routes

Routes may be protected from direct access. This might be necessary, e.g. when trying to keep classic customers from reaching Sky Q-specific steps.

They might also be used to whitelist routes, e.g. to prevent them from being automatically redirected to the homepage.

## Adding a protected route

The customer's state (classic/trial) is being stored in a secure [JWT token](http://jwt.io/).
To set a protected route, add them to the `trialSteps` array in `/get-config.js`.

## Whitelisting a route to allow direct access

By default, direct access to routes other than the home- and landing page is not permitted and will cause a redirect to home, unless the user has already been to the homepage (to discourage deep-linking from external sites).

This behavior can either be entirely deactivated, or specific URIs can be added to the whitelist array (`whitelistedPaths`) in `/get-config.js`.

## Redirecting legacy links to your site

If you find that other sites have legacy links to your application, you can add the url that is linked to the redirectLegacyDeepLinks array (`redirectLegacyDeepLinks`) in `/get-config.js`. These links are then redirected to the home page, rather than being redirected to the Sky 404 page.
