# Multi-Variant Testing

MVT is done through Adobe Target. The public JavaScript API described under [Feature Switches](./featureSwitches.md) can be used to toggle features based on the test scenario.

```javascript
window.setFeature({
  targetExperimentA: false,
  targetExperimentB: true,
})
```

### Current Tests

| Name              | Description               |
| ----------------- | ------------------------- |

<small>Please keep these up to date</small>
