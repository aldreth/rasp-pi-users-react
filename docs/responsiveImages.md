# Responsive images

The [picture component](src/shared/components/instructions/pods/picture.jsx) takes 3 different sized images, for desktop, tablet and phone, to produce a responsive picture element. The polyfill `picturefill.min.js` is included in the [`views/index.hbs`](views/index.hbs) template.

### Re-sizing Images

It is possible to produce the 3 correctly sized images by placing the full sized image in `original-images` and running:

```
gulp resize-images
```

3 resized, compressed and renamed images, `image-phone.jpg`, `image-tablet.jpg` & `image-desktop.jpg`, are placed into the `public/assets/img` directory.

This task requires GraphicsMagick and ImagageMagick to be installed:

```bash
brew install imagemagick
brew install graphicsmagick
```
