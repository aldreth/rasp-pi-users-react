# Application State

We're using [redux](redux.js.org) to store our application state.

![redux diagram](https://camo.githubusercontent.com/e7921fdb62c3bab89005e090677a6cd07aceaa8c/68747470733a2f2f7062732e7477696d672e636f6d2f6d656469612f434e50336b5953577741455672544a2e6a70673a6c61726765)
<small>react is our "View Provider"</small>


### Actions

Our actions follow the [Flux Standard Action](https://github.com/acdlite/flux-standard-action) format, e.g.:

```javascript
{
  type: 'SET_FEATURE',
  payload: {
    key: 'isTrialEnabled',
    value: true,
  }
}
```


### Reducers

We're using [Reducer Composition](http://redux.js.org/docs/basics/Reducers.html#splitting-reducers) (through `combineReducers()`) in [src/shared/reducers/index.js](src/shared/reducers/index.js).


### Store

The store is a singleton and is being injected at the root level. It must not be replaced at any point during the application lifecycle.

Upon change, the store's current state is being persisted into a cookie (that is being used to re-hydrate the store on refresh).


## Development Tools

We're using https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en (which embeds React DevTools) to observe, replay, and reset our application state.


## Example State

```json
{
  "features": {
    "isTrialEnabled": true,
    "isTargetEnabled": false,
    "targetExperimentA": true,
    "targetExperimentB": false,
    "targetExperimentC": false,
    "targetExperimentD": false
  },
  "blurryImage": {
    "tracking": "non-blurry",
    "url": "check-satellite-connection"
  },
  "location": {
    "latitude": 51.5,
    "longitude": -0.13,
    "isFetched": true
  },
  "weather": {
    "isSevere": false,
    "weatherData": [
      "rain",
      "snow",
      "thunderstorm",
      "wind"
    ],
    "isFetched": true
  },
  "user": {
    "isTrial": true
  }
}
```
