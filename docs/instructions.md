# Instructions

Routing is set up to pick up any modules under `src/shared/components/instructions/*`. The filenames become the instruction URL, e.g. `src/shared/components/instructions/home.jsx` becomes `/no-satellite-signal/home`.

## Building the instructions index

There's a gulp task (`gulp index:instructions`) that builds up an [index](../src/shared/components/instructions/index.js) of all react components under `src/shared/components/instructions/*`. This is then being consumed by the [Step component](../src/shared/containers/Step.jsx) to help with routing, some general analytics tasks, the page title, etc..


## Adding new instructions

To add a new instruction, simply place a new react component in `src/shared/components/instructions/*`.

If custom routes are required, they can still be added to the [routes](../src/shared/routes.jsx).
