Testing
-------
Uses [mountebank](http://www.mbtest.org/) for over the wire test doubles

### Run Unit Tests
Uses [jest](http://facebook.github.io/jest/)
* `gulp jest` to run

### Run Integration Tests
Mountebank and the application itself will be started and stopped automatically by the following commands.

##### Running against local selenium driver:
* `gulp nw:local --skip analytics` to run against local selenium driver

##### Running against browserstack:
* launch local browserstack tunnel binary with access key `./bin/BrowserStackLocalMac <AccessKey>`
* `gulp nw:bs --skip analytics` to run tests on browserstack
* //TODO configure multiple OS/Browsers to run on browserstack

##### Running analytics tests:
* `gulp nw:local --tags analytics --env proxy`
* analytics tests only run locally and not over browserstack
* Make sure JAVA_HOME is set correctly via `export JAVA_HOME=$(/usr/libexec/java_home)`

##### Errors running tests:
* `Error: connect ECONNREFUSED 127.0.0.1:2525` this is usually due to mountebank already running in the background. It may fail to run if it thinks a mountebank process is already running, therefore the following commands can help:

```
./mbkill
rm mb.pid
```
