# Feature Switches

Feature switches are a way of turning various bits of functionality within the application on or off without re-deploying.

There are 4 ways of setting a feature switch within Katana. These are (in order of precedence):

#### 1. Calling the public API on `window.setFeature()`

```javascript
window.setFeature('isTrialEnabled', true);
// or
window.setFeature({isTrialEnabled: true, someOtherFeature: false});
```

#### 2. Setting a cookie:

```javascript
document.cookie = "FEATURE_isTrialEnabled=true"
```

#### 3. Setting an environment variable:

```bash
FEATURE_isTrialEnabled=true gulp
```

#### 4. Hard coding the default value within `/get-config.js`

```javascript
// ...
features: {
  isTrialEnabled: true,
}
// ...
```

Each of these requires a name `FEATURE_NAME` (where `NAME` is the name of the feature and the value is `true` or `false`). In addition, the [public JavaScript API](#1-calling-the-public-api-on-windowsetfeature) also accepts an object hash of features.

The following features are currently available:

| Name            | Description               |
| --------------- | ------------------------- |
| isTrialEnabled  | Enables Sky Q integration |
| isTargetEnabled | Enables Adobe Target      |

There's also a list of current MVT-related feature switches under [Current Tests](./mvt.md#current-tests)


## Troubleshooting

#### My feature switches aren't being updated

The feature switches are being persisted in the [redux store](./applicationState.md#store). Clear the store (delete the `sky-katana-appState` cookie) and refresh.
